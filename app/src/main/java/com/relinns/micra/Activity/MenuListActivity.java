package com.relinns.micra.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra.Fragment.MyOrderNaviFragment;
import com.relinns.micra.Fragment.MessageListFragment;
import com.relinns.micra.Fragment.MyFavouriteFragment;
import com.relinns.micra.Fragment.MyHomeFragment;
import com.relinns.micra.Fragment.MyWalletFragment;
import com.relinns.micra.Fragment.ProfileNavigationSideWay;
import com.relinns.micra.Fragment.SettingFragment;
import com.relinns.micra.Fragment.WebMicraFragment;
import com.relinns.micra.Adapter.NavigationHomeAdapter;
import com.relinns.micra.R;
import com.relinns.micra.model.NavigationHomeModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MenuListActivity extends AppCompatActivity implements View.OnClickListener{

    ListView mDrawerList;
    RelativeLayout mainRL;
    Toolbar mToolbar;
    ActionBar actionBar;
    DrawerLayout mDrawerLayout;
    TextView titleTV,editTV,adnewEmployeTV,searchActionbarTV;
    ArrayList<NavigationHomeModel> navigationHome_models;
    android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mTitle;
    private float lastTranslate = 0.0f;
    ImageView drawer_open,notiText1,cartText1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu__list);
        titleTV = (TextView) findViewById(R.id.titleTV1);
        editTV = (TextView) findViewById(R.id.editTV);
        searchActionbarTV = (TextView) findViewById(R.id.search_actionbar);

        drawer_open=(ImageView)findViewById(R.id.drawer_open);
        notiText1 =(ImageView)findViewById(R.id.notiText1);
        cartText1=(ImageView)findViewById(R.id.cartText1);

        adnewEmployeTV = (TextView) findViewById(R.id.adnew_emplyTV);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer1);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mainRL = (RelativeLayout) findViewById(R.id.mainLL);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);



        String s1=getIntent().getExtras().getString("hlo1");


        if(s1.equals("1")) {

       Fragment favouriteFragment = new MyHomeFragment();
       FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
       fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
           fragmentTransaction.commit();

        }
        if(s1.equals("2")) {
            // Fragment favouriteFragment = new MyOrderFragment();
            Fragment favouriteFragment = new MyOrderNaviFragment();

            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
            fragmentTransaction.commit();
        }
        if(s1.equals("3")) {
            // Fragment favouriteFragment = new MyOrderFragment();
            Fragment favouriteFragment = new MyWalletFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
            fragmentTransaction.commit();
        }
        if(s1.equals("4")) {
            // Fragment favouriteFragment = new MyOrderFragment();
            Fragment favouriteFragment = new MyFavouriteFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
            fragmentTransaction.commit();
        }



        editTV.setOnClickListener(this);
        adnewEmployeTV.setOnClickListener(this);
        cartText1.setOnClickListener(this);
        notiText1.setOnClickListener(this);
        searchActionbarTV.setOnClickListener(this);
        actionBar = getSupportActionBar();
        setSupportActionBar(mToolbar);
        navigationHome_models = new ArrayList<NavigationHomeModel>();

        navigationHome_models.add(new NavigationHomeModel(R.drawable.home2, "Home"));
        navigationHome_models.add(new NavigationHomeModel(R.drawable.my_order, "My Orders"));
        navigationHome_models.add(new NavigationHomeModel(R.drawable.my_profile, "My Profile"));
        navigationHome_models.add(new NavigationHomeModel(R.drawable.favourite20, "Favourites"));
        navigationHome_models.add(new NavigationHomeModel(R.drawable.message, "Message"));

        navigationHome_models.add(new NavigationHomeModel(R.drawable.wallet, "My Wallet"));
        navigationHome_models.add(new NavigationHomeModel(R.drawable.store_employee, "Login Web Micra"));
        navigationHome_models.add(new NavigationHomeModel(R.drawable.settings, "Settings"));
        navigationHome_models.add(new NavigationHomeModel(R.drawable.logout, "Logout"));


        LayoutInflater inflater = getLayoutInflater();

        View listHeaderView = inflater.inflate(R.layout.header_list, null, false);

        ImageView drawer_profileIV = (CircleImageView) listHeaderView.findViewById(R.id.drawer_profileIV);
        TextView drawer_profilenameTV = (TextView) listHeaderView.findViewById(R.id.drawer_profilenameTV);

        // Glide.with(My_Products_Activity.this).load(persnl_image).centerCrop().crossFade().into(drawer_profileIV);
        //  drawer_profilenameTV.setText(persnl_name);

        mDrawerList.addHeaderView(listHeaderView);


        mDrawerList.setAdapter(new NavigationHomeAdapter(navigationHome_models,MenuListActivity.this));


        // productlist_Fragment();
       // overview_fragment();

        drawer_open.setOnClickListener(this);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        mDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this,
                mDrawerLayout,null,R.string.drawer_open,R.string.drawer_close  /* "close drawer" description */
        ) {
            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                InputMethodManager inputMethodManager = (InputMethodManager) MenuListActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(MenuListActivity.this.getCurrentFocus().getWindowToken(), 0);

                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mTitle);

                InputMethodManager inputMethodManager = (InputMethodManager) MenuListActivity.this
                        .getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(MenuListActivity.this.getCurrentFocus().getWindowToken(), 0);

                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }


            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);

                float moveFactor = (mDrawerList.getWidth() * slideOffset);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    mainRL.setTranslationX(moveFactor);
                } else {
                    TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                    anim.setDuration(0);
                    anim.setFillAfter(true);
                    mainRL.startAnimation(anim);

                    lastTranslate = moveFactor;
                }
            }

          /*  super.onDrawerOpened(drawerView);
            invalidateOptionsMenu();*/
        };


        getSupportActionBar().setTitle("");


        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);


//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);
////        getS
//         getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu1);
        mToolbar.setTitle("");
        mToolbar.setSubtitle("");


        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //  Toast.makeText(Navigation_Home_First.this, "=" + position, Toast.LENGTH_SHORT).show();
                switch (position) {
                    case 0:

                        break;

                    case 1:
                        mDrawerLayout.closeDrawers();
                        editTV.setVisibility(View.GONE);
                        adnewEmployeTV.setVisibility(View.GONE);
                        My_Home_fragment();
                        notiText1.setVisibility(View.VISIBLE);
                        cartText1.setVisibility(View.VISIBLE);
                        searchActionbarTV.setVisibility(View.GONE);

                       // overview_fragment();

                        break;

                    case 2:
                        mDrawerLayout.closeDrawers();
                        editTV.setVisibility(View.GONE);
                        adnewEmployeTV.setVisibility(View.GONE);
                        notiText1.setVisibility(View.GONE);
                        cartText1.setVisibility(View.GONE);
                        searchActionbarTV.setVisibility(View.VISIBLE);
                        My_Order_fragment();


                       // productlist_Fragment();



                        break;
                    case 3:
                       mDrawerLayout.closeDrawers();
                        editTV.setVisibility(View.VISIBLE);
                        adnewEmployeTV.setVisibility(View.GONE);
                        notiText1.setVisibility(View.GONE);
                        cartText1.setVisibility(View.GONE);
                        searchActionbarTV.setVisibility(View.GONE);

                        my_Profile_Fragment();

                        break;

                    case 4:

                        mDrawerLayout.closeDrawers();
                        editTV.setVisibility(View.GONE);
                        adnewEmployeTV.setVisibility(View.GONE);
                        notiText1.setVisibility(View.VISIBLE);
                        cartText1.setVisibility(View.VISIBLE);
                        searchActionbarTV.setVisibility(View.GONE);

                        my_Favourite_Fragment();

                       /* Intent intent = new Intent(My_Products_Activity.this, Add_StoreAddress_Activity.class);
                        startActivity(intent);*/

                        break;

                    case 5:
                      mDrawerLayout.closeDrawers();
                        editTV.setVisibility(View.GONE);
                        adnewEmployeTV.setVisibility(View.GONE);
                        notiText1.setVisibility(View.GONE);
                        cartText1.setVisibility(View.GONE);
                        searchActionbarTV.setVisibility(View.GONE);

                        my_message_Fragment();
                        break;

                    case 6:
                       mDrawerLayout.closeDrawers();
                        editTV.setVisibility(View.GONE);
                        adnewEmployeTV.setVisibility(View.GONE);
                        notiText1.setVisibility(View.GONE);
                        cartText1.setVisibility(View.GONE);
                        searchActionbarTV.setVisibility(View.GONE);

                        My_wallet_fragment();


                        break;
                    case 7:

                        mDrawerLayout.closeDrawers();
                        editTV.setVisibility(View.GONE);
                        adnewEmployeTV.setVisibility(View.GONE);
                        notiText1.setVisibility(View.GONE);
                        cartText1.setVisibility(View.GONE);
                        searchActionbarTV.setVisibility(View.GONE);

                        Login_web_fragment();

                        break;

                   case 8:

                       mDrawerLayout.closeDrawers();
                       editTV.setVisibility(View.GONE);
                       adnewEmployeTV.setVisibility(View.GONE);
                       notiText1.setVisibility(View.GONE);
                       cartText1.setVisibility(View.GONE);
                       searchActionbarTV.setVisibility(View.GONE);

                       My_Setting_fragment();




                       break;
                    case 9:
                        mDrawerLayout.closeDrawers();
                        editTV.setVisibility(View.GONE);
                        adnewEmployeTV.setVisibility(View.GONE);
                        notiText1.setVisibility(View.GONE);
                        cartText1.setVisibility(View.GONE);
                        searchActionbarTV.setVisibility(View.GONE);

                        My_logout_fragment();
                        break;

                }
            }

            private void My_wallet_fragment() {
                Fragment favouriteFragment = new MyWalletFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
                fragmentTransaction.commit();
            }
            private void My_Home_fragment() {
                Fragment favouriteFragment = new MyHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
                fragmentTransaction.commit();
            }
            private void My_Order_fragment() {
               // Fragment favouriteFragment = new MyOrderFragment();
                Fragment favouriteFragment = new MyOrderNaviFragment();

                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
                fragmentTransaction.commit();
            }
            private void my_Profile_Fragment() {
                //Fragment favouriteFragment = new MyProfile_Fragment();
                Fragment favouriteFragment = new ProfileNavigationSideWay();

                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
                fragmentTransaction.commit();
            }
            private void my_Favourite_Fragment() {
                Fragment favouriteFragment = new MyFavouriteFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
                fragmentTransaction.commit();
            }
            private void my_message_Fragment() {
                Fragment favouriteFragment = new MessageListFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
                fragmentTransaction.commit();
            }
            private void Login_web_fragment() {
                Fragment favouriteFragment = new WebMicraFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
                fragmentTransaction.commit();
            }
            private void My_Setting_fragment() {
                Fragment favouriteFragment = new SettingFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
                fragmentTransaction.commit();
            }
            private void My_logout_fragment() {
          Intent i=new Intent(MenuListActivity.this,LoginActivity.class);
                startActivity(i);
                finish();
            }

        });


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.editTV:
                Intent intent=new Intent(MenuListActivity.this,EditProfileActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exit);
                break;
            case R.id.adnew_emplyTV:
                Intent intent1=new Intent( MenuListActivity.this,EditProfileActivity.class);
                startActivity(intent1);
                overridePendingTransition(R.anim.enter,R.anim.exit);
                break;
            case R.id.drawer_open:
                mDrawerLayout.openDrawer(Gravity.LEFT);
                break;

            case R.id.cartText1:
                Intent intent2=new Intent( MenuListActivity.this,CartActivity.class);
                startActivity(intent2);
                break;
            case R.id.notiText1:
                Intent intent3=new Intent( MenuListActivity.this,NotificationListActivity.class);
                startActivity(intent3);
                break;
            case R.id.search_actionbar:
                Intent intent4=new Intent( MenuListActivity.this,Main7Activity.class);
                startActivity(intent4);
                break;


        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        }
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {

            if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            } else {
                mDrawerLayout.openDrawer(GravityCompat.START);
            }

        }
        return super.onOptionsItemSelected(item);
    }


    public void settitleactivity(String title){
        //    getSupportActionBar().setTitle(title);
        titleTV.setText(title);
    }

    @Override
    public void onBackPressed() {
        exit();
    }

    public void exit() {
        AlertDialog.Builder a = new AlertDialog.Builder(MenuListActivity.this);
        a.setMessage("Do You Want To Exit ");
        a.setCancelable(true);
        a.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent setIntent = new Intent(Intent.ACTION_MAIN);
                setIntent.addCategory(Intent.CATEGORY_HOME);
                setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(setIntent);
                MenuListActivity.this.finish();
            }
        });
        a.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });


        AlertDialog alertDialog = a.create();
        alertDialog.show();
    }

    }

