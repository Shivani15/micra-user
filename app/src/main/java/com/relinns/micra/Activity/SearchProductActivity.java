package com.relinns.micra.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.relinns.micra.Adapter.SearchProductAdapter;
import com.relinns.micra.R;
import com.relinns.micra.model.SearchProductModel;

import java.util.ArrayList;

public class SearchProductActivity extends AppCompatActivity implements  View.OnClickListener {
ListView lv;
    ArrayList<SearchProductModel> arraylist1;
    SearchProductAdapter spaAdapter;
    SearchProductModel spmModel;
    ImageView backPressIV,notification1IV,cartText1IV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search__product);
        lv=(ListView)findViewById(R.id.list1);
        backPressIV=(ImageView)findViewById(R.id.notification1);
        notification1IV=(ImageView)findViewById(R.id.backPress);
        cartText1IV=(ImageView)findViewById(R.id.cartText1);
        backPressIV.setOnClickListener(this);
        notification1IV.setOnClickListener(this);
        cartText1IV.setOnClickListener(this);

        arraylist1=new ArrayList<>();
        arraylist1.add(new SearchProductModel("Product Name","Price",R.drawable.winter));
        arraylist1.add(new SearchProductModel("Product Name","Price",R.drawable.winter));
        arraylist1.add(new SearchProductModel("Product Name","Price",R.drawable.winter));
        arraylist1.add(new SearchProductModel("Product Name","Price",R.drawable.winter));

        spaAdapter=new SearchProductAdapter(arraylist1,SearchProductActivity.this);
        lv.setAdapter(spaAdapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.backPress:
                finish();
                break;
            case R.id.notification1:
                Intent i1=new Intent(SearchProductActivity.this,NotificationListActivity.class);
                startActivity(i1);
                break;
            case R.id.cartText1:
                Intent i2=new Intent(SearchProductActivity.this,CartActivity.class);
                startActivity(i2);
                break;
        }
    }
}
