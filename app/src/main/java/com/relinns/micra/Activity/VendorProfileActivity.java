package com.relinns.micra.Activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.relinns.micra.Fragment.ProfileVendorFragment;
import com.relinns.micra.R;


public class VendorProfileActivity extends AppCompatActivity {
    TextView productsTV, ordersTV;
    Button btn1;
    ImageView backPressIV, cartText1IV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_profile);
        productsTV = (TextView) findViewById(R.id.product1);
        ordersTV = (TextView) findViewById(R.id.Order1);
        btn1 = (Button) findViewById(R.id.uplandorder1);
        backPressIV = (ImageView) findViewById(R.id.backPress);
        cartText1IV = (ImageView) findViewById(R.id.cartText1);
        cartText1IV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(VendorProfileActivity.this, CartActivity.class);
                startActivity(i);
            }
        });

        backPressIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        productsTV.setTextColor(getResources().getColor(R.color.white));
        productsTV.setBackgroundColor(getResources().getColor(R.color.micra));

        Fragment hello = new ProfileVendorFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.Frame1, hello, null);
        fragmentTransaction.commit();

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(VendorProfileActivity.this, UploadPlaceOrderActivity.class);
                startActivity(i);
            }
        });


        productsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productsTV.setBackgroundColor(getResources().getColor(R.color.micra));
                productsTV.setTextColor(getResources().getColor(R.color.white));
                ordersTV.setTextColor(getResources().getColor(R.color.black));
                ordersTV.setBackgroundColor(getResources().getColor(R.color.white));

                Fragment hello = new ProfileVendorFragment();
                android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.Frame1, hello, null);
                fragmentTransaction.commit();

            }
        });
        ordersTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ordersTV.setBackgroundColor(getResources().getColor(R.color.micra));
                ordersTV.setTextColor(getResources().getColor(R.color.white));
                productsTV.setTextColor(getResources().getColor(R.color.black));
                productsTV.setBackgroundColor(getResources().getColor(R.color.white));

                Fragment hello = new ProfileVendorFragment();
                android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.Frame1, hello, null);
                fragmentTransaction.commit();

            }
        });
    }
}
