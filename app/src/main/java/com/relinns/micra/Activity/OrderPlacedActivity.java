package com.relinns.micra.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.relinns.micra.R;

public class OrderPlacedActivity extends AppCompatActivity {
LinearLayout lmLL,lm1LL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_placed);
        lmLL=(LinearLayout)findViewById(R.id.another);
        lm1LL=(LinearLayout)findViewById(R.id.place_another_order2);
        lm1LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i= new Intent(OrderPlacedActivity.this,MenuListActivity.class);
                i.putExtra("hlo1","1");

                startActivity(i);
            }
        });

    }
    public  void place_another(View v)
    {
        Intent i= new Intent(OrderPlacedActivity.this,MenuListActivity.class);
        i.putExtra("hlo1","2");

        startActivity(i);
    }
    public void backpress(View v)
    {
        finish();
    }
}
