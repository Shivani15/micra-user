package com.relinns.micra.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import com.relinns.micra.R;

public class TopUpActivity extends AppCompatActivity {
    Spinner s1Spin, s2Spin;
    String[] sam1 = {"Debit Card", "b", "c", "d"};
    String[] sam2 = {"xxxxxxxxxxxxxxxxxx4056", "b", "c", "d"};
    ImageView backPressIV;
    ArrayAdapter adapter, adapter1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.topup);
        s1Spin = (Spinner) findViewById(R.id.spinner_method);
        s2Spin = (Spinner) findViewById(R.id.spinner_card);
        backPressIV = (ImageView) findViewById(R.id.backPress);
        backPressIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, sam1);
        s1Spin.setAdapter(adapter);
        adapter1 = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, sam2);
        s2Spin.setAdapter(adapter1);


    }
}
