package com.relinns.micra.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 29-06-2017.
 */
public class OrderDetailPendingActivity extends Activity implements View.OnClickListener{
    RelativeLayout backRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.orderdetail_pending);
        backRL=(RelativeLayout)findViewById(R.id.backRL);


        backRL.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        OrderDetailPendingActivity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
