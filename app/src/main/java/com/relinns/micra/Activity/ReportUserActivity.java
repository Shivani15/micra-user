package com.relinns.micra.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 04-07-2017.
 */
public class ReportUserActivity extends Activity implements View.OnClickListener {
    RelativeLayout backRL;
    Button btn1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_reportuser);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        btn1=(Button)findViewById(R.id.send_report_User);
        backRL.setOnClickListener(this);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i=new Intent(ReportUserActivity.this,MenuListActivity.class);
//                startActivity(i);
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.backRL:

                onBackPressed();


                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ReportUserActivity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
