package com.relinns.micra.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 22-06-2017.
 */
public class ProductDetailsActivity extends Activity implements View.OnClickListener{

    RelativeLayout backRL,allRL;
    Button btn1;
    ImageView notiProductDetailIV,dots3IV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productdetails);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        btn1=(Button)findViewById(R.id.view_All_Reviews);
        notiProductDetailIV=(ImageView)findViewById(R.id.noti_product_detail);
        dots3IV=(ImageView)findViewById(R.id.dots3);
        allRL=(RelativeLayout)findViewById(R.id.off1);

        Intent intent = getIntent();

        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey("image")) {
                String s1=getIntent().getStringExtra("image");

                if (s1.equals("1")) {
                    dots3IV.setVisibility(View.GONE);
                }
                else
                {
                    dots3IV.setVisibility(View.VISIBLE);

                }
            }

        }
        else {
            dots3IV.setVisibility(View.VISIBLE);
        }


      //  String s1=getIntent().getExtras().getString("image");

//            if (s1.equals("1")) {
//                dots3.setVisibility(View.GONE);
//            }
//            else
//            {
//                dots3.setVisibility(View.VISIBLE);
//
//            }


        backRL.setOnClickListener(this);
        btn1.setOnClickListener(this);
        notiProductDetailIV.setOnClickListener(this);
        allRL.setOnClickListener(this);
        dots3IV.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:

                onBackPressed();
                break;
            case R.id.view_All_Reviews:
                Intent i=new Intent(ProductDetailsActivity.this,Main6Activity.class);
                startActivity(i);
                break;
            case R.id.off1:
                Intent i1=new Intent(ProductDetailsActivity.this,VendorProfileActivity.class);
                startActivity(i1);
                break;
            case R.id.noti_product_detail:
                Intent i2=new Intent(ProductDetailsActivity.this,CartActivity.class);
                startActivity(i2);
                break;
            case R.id.dots3:
                final Dialog dialog=new Dialog(this);
                dialog.setContentView(R.layout.product_details_dialog);
                TextView tv=(TextView)dialog.findViewById(R.id.reportabuse1);
                Window dialogWindow = dialog.getWindow();
               WindowManager.LayoutParams wmlp = dialogWindow.getAttributes();
                //dialogWindow.setGravity(Gravity.RIGHT | Gravity.TOP);
                wmlp.gravity = Gravity.TOP | Gravity.RIGHT;
                wmlp.x = 20;   //x position
                wmlp.y = 60;   //y position

                tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent i4=new Intent(ProductDetailsActivity.this,Main5Activity.class);
                        startActivity(i4);
                        //finish();
                    }
                });
                dialog.show();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ProductDetailsActivity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
