package com.relinns.micra.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 04-07-2017.
 */
public class ReportFeedbackActivity extends Activity  {

    Button btn1;
    RelativeLayout bacurlRL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reportfeedback);
        btn1=(Button)findViewById(R.id.sendBT);
        bacurlRL=(RelativeLayout)findViewById(R.id.backRL);
        bacurlRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


}
