package com.relinns.micra.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.relinns.micra.R;

public class PaymentActivity extends AppCompatActivity {
  LinearLayout pay1LL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        pay1LL=(LinearLayout)findViewById(R.id.pay2);
    }
    public void pay_money(View v)
    {
        Intent i= new Intent(PaymentActivity.this,OrderPlacedActivity.class);
        startActivity(i);
    }
    public void backpress(View v)
    {
        finish();
    }
}
