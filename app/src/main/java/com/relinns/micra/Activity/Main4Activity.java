package com.relinns.micra.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import com.relinns.micra.R;

public class Main4Activity extends AppCompatActivity {
    String[] s={"Received Expired Products","Received","Received","Received"};
    ArrayAdapter adapter;
    Spinner spSpin;
    Button b1Btn;
    ImageView backPress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main44);
        spSpin=(Spinner)findViewById(R.id.spinr);
        b1Btn=(Button)findViewById(R.id.submit1);
        backPress=(ImageView)findViewById(R.id.backPress);
        backPress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        adapter=new ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item,s);
        spSpin.setAdapter(adapter);
        b1Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Main4Activity.this,RequestFundAreaActivity.class);
                startActivity(i);
            }
        });

    }

}
