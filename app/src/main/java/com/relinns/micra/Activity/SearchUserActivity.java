package com.relinns.micra.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.relinns.micra.Adapter.SearchUserAdapter;
import com.relinns.micra.R;
import com.relinns.micra.model.UserModel;

import java.util.ArrayList;


public class SearchUserActivity extends AppCompatActivity implements View.OnClickListener {
    ListView lv;
    ImageView cartText1IV;
    ArrayList<UserModel> userlist;
    ImageView backPressIV;
    SearchUserAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search__user);
        lv = (ListView) findViewById(R.id.list1);
        backPressIV = (ImageView) findViewById(R.id.backPress);
        cartText1IV = (ImageView) findViewById(R.id.cartText1);
        cartText1IV.setOnClickListener(this);
        backPressIV.setOnClickListener(this);


        userlist = new ArrayList<>();
        userlist.add(new UserModel(R.drawable.face1, "John Doe", "3 km away", "Mohali,India"));
        userlist.add(new UserModel(R.drawable.face1, "John Doe", "3 km away", "Mohali,India"));
        userlist.add(new UserModel(R.drawable.face1, "John Doe", "3 km away", "Mohali,India"));
        userlist.add(new UserModel(R.drawable.face1, "John Doe", "3 km away", "Mohali,India"));

        adapter = new SearchUserAdapter(this, userlist);
        lv.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backPress:
                finish();
                break;
            case R.id.cartText1:
                Intent i1 = new Intent(SearchUserActivity.this, CartActivity.class);
                startActivity(i1);
        }
    }
}

