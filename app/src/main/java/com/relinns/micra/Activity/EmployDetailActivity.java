package com.relinns.micra.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 03-07-2017.
 */
public class EmployDetailActivity extends Activity implements View.OnClickListener {
    RelativeLayout backRL,trackemployeeRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employeedetail);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        trackemployeeRL=(RelativeLayout)findViewById(R.id.trackemployeeRL);
        backRL.setOnClickListener(this);
        trackemployeeRL.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;
            case R.id.trackemployeeRL:
//                Intent intent=new Intent(EmployDetailActivity.this,NotificationListActivity.class);
//                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exit);
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        EmployDetailActivity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
