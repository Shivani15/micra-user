package com.relinns.micra.Activity;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.relinns.micra.Adapter.MessageAdapter;
import com.relinns.micra.R;
import com.relinns.micra.model.MessageModel;

import java.util.ArrayList;

public class MessageActivity extends Fragment {
    MessageAdapter message_adapter;
    ArrayList<MessageModel>   employlistLV1;
    ListView employLV;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View v=inflater.inflate(R.layout.activity_message,container,false);
        ListView employ=(ListView)v.findViewById(R.id.employlistLV);


        employlistLV1=new ArrayList<>();
        employlistLV1.add(new MessageModel(R.drawable.offline_user1,"John Doe","Mohali,India"));
        employlistLV1.add(new MessageModel(R.drawable.offline_user1,"John Doe","Mohali,India"));
        employlistLV1.add(new MessageModel(R.drawable.offline_user1,"John Doe","Mohali,India"));
        message_adapter=new MessageAdapter(getActivity(),employlistLV1);
        employ.setAdapter(message_adapter);

        return v;
    }

}
