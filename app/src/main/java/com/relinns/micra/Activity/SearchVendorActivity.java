package com.relinns.micra.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.relinns.micra.Adapter.SearchVendorAdapter;
import com.relinns.micra.R;
import com.relinns.micra.model.VendorModel;

import java.util.ArrayList;


public class SearchVendorActivity extends AppCompatActivity implements  View.OnClickListener{

    ListView lv;
    ImageView backPressIV,cartText1IV;
    ArrayList<VendorModel> vendorlist;

    SearchVendorAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search__vendor);
        lv=(ListView)findViewById(R.id.list1);
        backPressIV=(ImageView)findViewById(R.id.backPress);
        cartText1IV=(ImageView)findViewById(R.id.cartText1);
        cartText1IV.setOnClickListener(this);
        backPressIV.setOnClickListener(this);

        backPressIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        vendorlist=new ArrayList<>();
        vendorlist.add(new VendorModel(R.drawable.face1,"Sai Medical Store","3 km away","PaymentActivity:Mode,Code Wallet"));
        vendorlist.add(new VendorModel(R.drawable.face1,"Sai Medical Store","3 km away","PaymentActivity:Mode,Code Wallet"));
        vendorlist.add(new VendorModel(R.drawable.face1,"Sai Medical Store","3 km away","PaymentActivity:Mode,Code Wallet"));
        vendorlist.add(new VendorModel(R.drawable.face1,"Sai Medical Store","3 km away","PaymentActivity:Mode,Code Wallet"));

        adapter=new SearchVendorAdapter(this,vendorlist);
        lv.setAdapter(adapter);
}

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backPress:
                finish();
                break;
            case R.id.cartText1:
                Intent i1 = new Intent(SearchVendorActivity.this, CartActivity.class);
                startActivity(i1);
    }
}}
