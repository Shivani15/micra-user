package com.relinns.micra.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;

import com.relinns.micra.Adapter.BrowseProductAdapter;
import com.relinns.micra.R;
import com.relinns.micra.model.BrowseProductModel;

import java.util.ArrayList;

public class BrowseProductActivity extends AppCompatActivity implements View.OnClickListener{
    ListView lv;
    String[] s={"All Category","a","b"};
    ArrayAdapter adapter;
    ImageView backpressImageView,cartText1ImageView;

    ArrayList<BrowseProductModel> arraylist1;
    BrowseProductAdapter spa_adapter;
    BrowseProductModel spm_model;
    Spinner s1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse__product);
        lv=(ListView)findViewById(R.id.list1);
        s1=(Spinner)findViewById(R.id.spiner1);
        backpressImageView=(ImageView)findViewById(R.id.backPress);
        cartText1ImageView=(ImageView)findViewById(R.id.cartText1);
        backpressImageView.setOnClickListener(this);
        cartText1ImageView.setOnClickListener(this);
        adapter=new ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item,s);
        s1.setAdapter(adapter);
        arraylist1=new ArrayList<>();
        arraylist1.add(new BrowseProductModel("Product Name","Price",R.drawable.winter));
        arraylist1.add(new BrowseProductModel("Product Name","Price",R.drawable.winter));
        arraylist1.add(new BrowseProductModel("Product Name","Price",R.drawable.winter));
        arraylist1.add(new BrowseProductModel("Product Name","Price",R.drawable.winter));

        spa_adapter=new BrowseProductAdapter(arraylist1,BrowseProductActivity.this);
        lv.setAdapter(spa_adapter);

    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.backPress:
                finish();
                break;
            case R.id.cartText1:
                Intent i=new Intent(BrowseProductActivity.this,CartActivity.class);
                startActivity(i);
                break;
        }
    }
    }

