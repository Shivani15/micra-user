package com.relinns.micra.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.relinns.micra.Adapter.BlockUsersAdapter;
import com.relinns.micra.R;


public class BlockedUserActivity extends AppCompatActivity  {
    ListView blockuserLV;
    BlockUsersAdapter blockUsers_adapter;
    RelativeLayout backurRLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocked_user);
        blockuserLV=(ListView)findViewById(R.id.blockuserLV);
        backurRLayout=(RelativeLayout) findViewById(R.id.backRL);
        backurRLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        blockUsers_adapter=new BlockUsersAdapter(BlockedUserActivity.this);
        blockuserLV.setAdapter(blockUsers_adapter);

    }


}
