package com.relinns.micra.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.relinns.micra.Adapter.CustomerCartAdapter;
import com.relinns.micra.R;
import com.relinns.micra.model.CartModel;

import java.util.ArrayList;

public class CartActivity extends AppCompatActivity {
    ArrayList<CartModel> cart_list;
    CustomerCartAdapter cartAdapter;
    RecyclerView cartRecyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        cart_list=new ArrayList<>();
        cartRecyclerView=(RecyclerView)findViewById(R.id.cart_listView);
        cart_list.add(new CartModel(R.drawable.placeholder,"Product Name","Vendor Name","575.60","Buy 3 Get 1 free","2"));
        cart_list.add(new CartModel(R.drawable.placeholder,"Product Name","Vendor Name","575.60","Buy 3 Get 1 free","2"));
        cart_list.add(new CartModel(R.drawable.placeholder,"Product Name","Vendor Name","575.60","Buy 3 Get 1 free","2"));
        cart_list.add(new CartModel(R.drawable.placeholder,"Product Name","Vendor Name","575.60","Buy 3 Get 1 free","2"));
        cart_list.add(new CartModel(R.drawable.placeholder,"Product Name","Vendor Name","575.60","Buy 3 Get 1 free","2"));
        cart_list.add(new CartModel(R.drawable.placeholder,"Product Name","Vendor Name","575.60","Buy 3 Get 1 free","2"));
        cart_list.add(new CartModel(R.drawable.placeholder,"Product Name","Vendor Name","575.60","Buy 3 Get 1 free","2"));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CartActivity.this);
        cartRecyclerView.setLayoutManager(mLayoutManager);
        cartAdapter=new CustomerCartAdapter(CartActivity.this,cart_list);
        cartRecyclerView.setAdapter(cartAdapter);
    }
    public void checkout(View v)
    {
        Intent i= new Intent(CartActivity.this,CheckoutActivity.class);
        startActivity(i);
    }
    public void backpress(View v)
    {
        finish();
    }

    }

