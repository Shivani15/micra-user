package com.relinns.micra.Activity;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra.R;

public class OrderActivity extends AppCompatActivity {

    Button btn1;
    RelativeLayout rLayout1;
    TextView tView1;
    ImageView backPressIV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order);
        rLayout1=(RelativeLayout)findViewById(R.id.rel3);
        tView1=(TextView)findViewById(R.id.approv2);
        btn1=(Button)findViewById(R.id.cancelorder1);
        backPressIV=(ImageView)findViewById(R.id.backPress);
        backPressIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        new CountDownTimer(5000, 1000) {

            public void onTick(long millisUntilFinished) {


            }

            public void onFinish() {
                rLayout1.setBackground(getResources().getDrawable(R.drawable.circle1));
                tView1.setTextColor(getResources().getColor(R.color.micra));


            }

        }.start();
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(OrderActivity.this,OrderPartActivity.class);
                startActivity(i);
            }
        });

    }
}
