package com.relinns.micra.Activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.relinns.micra.Fragment.Fragment1;
import com.relinns.micra.R;

public class MainActivity extends AppCompatActivity {
   TextView b1TV,b2TV;
    ImageView backpressIV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        backpressIV=(ImageView)findViewById(R.id.backPress);
        b1TV=(TextView)findViewById(R.id.about1);
        b2TV=(TextView)findViewById(R.id.frend1);
        backpressIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Fragment hello = new Fragment1();
        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frag1, hello,null);
        fragmentTransaction.commit();
        b1TV.setBackgroundColor(getResources().getColor(R.color.micra));
        b1TV.setTextColor(getResources().getColor(R.color.white));
        b2TV.setTextColor(getResources().getColor(R.color.black));
        b2TV.setBackgroundColor(getResources().getColor(R.color.white));



        b1TV.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            b1TV.setBackgroundColor(getResources().getColor(R.color.micra));
            b1TV.setTextColor(getResources().getColor(R.color.white));
            b2TV.setTextColor(getResources().getColor(R.color.black));
            b2TV.setBackgroundColor(getResources().getColor(R.color.white));

            Fragment hello = new Fragment1();
            android.support.v4.app.FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frag1, hello,null);
            fragmentTransaction.commit();

        }
    });
        b2TV.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            b2TV.setBackgroundColor(getResources().getColor(R.color.micra));
            b2TV.setTextColor(getResources().getColor(R.color.white));
            b1TV.setTextColor(getResources().getColor(R.color.black));
            b1TV.setBackgroundColor(getResources().getColor(R.color.white));

            Fragment hello = new Fragment1();
            android.support.v4.app.FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frag1, hello,null);
            fragmentTransaction.commit();

        }
    });}
}
