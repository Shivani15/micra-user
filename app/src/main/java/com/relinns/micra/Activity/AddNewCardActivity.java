package com.relinns.micra.Activity;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RelativeLayout;

import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 06-07-2017.
 */

public class AddNewCardActivity extends Activity  implements View.OnClickListener{

    RelativeLayout backRL;
    RelativeLayout cardRlayout;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addnewcard);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        cardRlayout=(RelativeLayout)findViewById(R.id.newcard);

        backRL.setOnClickListener(this);
        cardRlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i=new Intent(AddNewCardActivity.this,EditCardActivity.class);
//                startActivity(i);
            }
        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        AddNewCardActivity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
