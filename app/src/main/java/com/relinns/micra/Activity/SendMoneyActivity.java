package com.relinns.micra.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RelativeLayout;

import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 07-07-2017.
 */

public class SendMoneyActivity extends Activity implements View.OnClickListener {
    RelativeLayout backRL;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sendmoney);
        backRL=(RelativeLayout)findViewById(R.id.backRL);

        backRL.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        SendMoneyActivity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
