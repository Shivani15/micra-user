package com.relinns.micra.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.relinns.micra.R;

public class CheckoutActivity extends AppCompatActivity {
LinearLayout contnLL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        contnLL=(LinearLayout)findViewById(R.id.cont);
    }
    public void continue_payment(View v)
    {
        Intent i= new Intent(CheckoutActivity.this,PaymentActivity.class);
        startActivity(i);
    }
    public void backpress(View v)
    {
        finish();
    }
}
