package com.relinns.micra.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RelativeLayout;

import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 06-07-2017.
 */

public class EditBankActivity extends Activity implements View.OnClickListener {
    RelativeLayout backRL;
    RelativeLayout b1RL;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editbank);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        b1RL=(RelativeLayout)findViewById(R.id.editbak1);

        backRL.setOnClickListener(this);
        b1RL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i=new Intent(EditBankActivity.this,TopUpActivity.class);
//                startActivity(i);
            }
        });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        EditBankActivity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
