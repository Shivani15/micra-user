package com.relinns.micra.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.relinns.micra.R;

public class LeaveApplicationActivity extends Activity implements View.OnClickListener{
    RelativeLayout backRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_application_activitry);
        backRL=(RelativeLayout)findViewById(R.id.backRL);

        backRL.setOnClickListener(this);
    }
    public void open_send(View v){
        Intent intent = new Intent(LeaveApplicationActivity.this,MainActivity.class);
        startActivity(intent);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;
        }

    }
}
