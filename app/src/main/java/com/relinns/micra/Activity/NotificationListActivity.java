package com.relinns.micra.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.relinns.micra.Adapter.NotificationListAdapter;
import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 04-07-2017.
 */
public class NotificationListActivity extends Activity implements View.OnClickListener{

    ListView notificationLV;
    NotificationListAdapter notificationList_adapter;
    RelativeLayout backRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificationlist);
        notificationLV=(ListView)findViewById(R.id.notificationLV);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        backRL.setOnClickListener(this);

        notificationList_adapter=new NotificationListAdapter(NotificationListActivity.this);
        notificationLV.setAdapter(notificationList_adapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        NotificationListActivity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
