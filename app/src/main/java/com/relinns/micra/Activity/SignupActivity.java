package com.relinns.micra.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.relinns.micra.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class SignupActivity extends AppCompatActivity {

    Dialog dialog;
    ListView listView;
    String items[],items1[];
    TextView cCodeTV,lineTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_);
        code();
        cCodeTV = (TextView)findViewById(R.id.c_code);
        lineTV = (TextView)findViewById(R.id.line);
        line_color();

    }


    public void open_verification(View view){

        Intent intent = new Intent(SignupActivity.this,VerificationActivity.class);
        startActivity(intent);

    }


    public void backpress(View view){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    void line_color(){

        SpannableString SpanString = new SpannableString("By clicking 'Register' you agree to Terms & Conditions and Privacy Policy");

        ClickableSpan teremsAndCondition = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

                Intent mIntent = new Intent(SignupActivity.this, AboutusActivity.class);
               // mIntent.putExtra("isTermsAndCondition", true);
                mIntent.putExtra("about","Terms of use");
                startActivity(mIntent);

            }
        };

        ClickableSpan privacy = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

                Intent mIntent = new Intent(SignupActivity.this, AboutusActivity.class);
                //mIntent.putExtra("isPrivacyPolicy", true);
                mIntent.putExtra("about","Privacy Policy");

                startActivity(mIntent);

            }
        };

        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.micra)), 12, 22, 0);
        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.micra)), 36, 54, 0);
        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.micra)), 59, 73, 0);

        lineTV.setMovementMethod(LinkMovementMethod.getInstance());
        lineTV.setText(SpanString, TextView.BufferType.SPANNABLE);
        lineTV.setSelected(true);


    }

    void code(){

        try {
            JSONArray jsonArray=new JSONArray(loadJSONFromAsset());
            items=new String[jsonArray.length()];
            items1=new String[jsonArray.length()];
            for(int i=0;i<jsonArray.length();i++){
                JSONObject object=jsonArray.getJSONObject(i);
                Log.d("response",object+"");
                items[i]="+"+object.getString("code")+" "+object.getString("name");
                items1[i] = "+"+object.getString("code");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String loadJSONFromAsset() {
        String json = null;
        try {

            InputStream is = getAssets().open("isd.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    public void open_code(View view)
    {
        // DialogWithBlurredBackgroundLauncher dialogWithBlurredBackgroundLauncher = new DialogWithBlurredBackgroundLauncher(MainActivity.this);
        dialog = new Dialog(SignupActivity.this);
        // Include dialog.xml file

        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes(); // retrieves the windows attributes

        lp.dimAmount=0.0f; // sets the dimming amount to zero

        dialog.getWindow().setAttributes(lp); // sets the updated windows attributes

        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.countrydesign);



        listView=(ListView) dialog.findViewById(R.id.list);
        //listView.setFastScrollEnabled(true);

        ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        listView.setAdapter(itemsAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                cCodeTV.setText(items1[position]);

                dialog.dismiss();
            }
        });

        dialog.show();
    }




}
