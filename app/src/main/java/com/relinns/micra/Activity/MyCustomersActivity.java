package com.relinns.micra.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import com.relinns.micra.Adapter.MyCustomerAdapter;
import com.relinns.micra.R;
import com.relinns.micra.model.CustomerModel;

import java.util.ArrayList;

public class MyCustomersActivity extends AppCompatActivity {
    ArrayList<CustomerModel> customer_data;
    RecyclerView myCustomerRecyclerView;
    MyCustomerAdapter customerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_customers);
        myCustomerRecyclerView = (RecyclerView) findViewById(R.id.my_customerlist);
        customer_data = new ArrayList<>();
        customer_data.add(new CustomerModel(R.drawable.offline_user, "John Doe", "City, Country", "0"));
        customer_data.add(new CustomerModel(R.drawable.offline_user, "John Doe", "City, Country", "0"));
        customer_data.add(new CustomerModel(R.drawable.offline_user, "John Doe", "City, Country", "1"));
        customer_data.add(new CustomerModel(R.drawable.offline_user, "John Doe", "City, Country", "0"));
        customer_data.add(new CustomerModel(R.drawable.offline_user, "John Doe", "City, Country", "1"));

//
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MyCustomersActivity.this);
//        my_customerlist.setLayoutManager(mLayoutManager);
//        customerAdapter = new MyCustomerAdapter(MyCustomersActivity.this, customer_data);
//        my_customerlist.setAdapter(customerAdapter);
    }
}
