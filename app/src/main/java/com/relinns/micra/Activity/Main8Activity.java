package com.relinns.micra.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.relinns.micra.Adapter.MyAdapter;
import com.relinns.micra.R;

public class Main8Activity extends AppCompatActivity implements  View.OnClickListener {
 ListView lv;
    String[] s1={"John Doe","John Doe"};
    String [] s2={"John","John"};
    Integer[] img={R.drawable.winter, R.drawable.winter};
    ImageView backPressIV,cartText1IV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main8);
       // getSupportActionBar().hide();
        lv=(ListView)findViewById(R.id.list1);
        backPressIV=(ImageView)findViewById(R.id.backPress);
        cartText1IV=(ImageView)findViewById(R.id.cartText1);


        backPressIV.setOnClickListener(this);
        cartText1IV.setOnClickListener(this);
        MyAdapter adapter=new MyAdapter(this,s1,s2,img);
        lv.setAdapter(adapter);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.backPress:
                finish();
            case R.id.cartText1:
                Intent i=new Intent(Main8Activity.this,CartActivity.class);
                startActivity(i);
        }
    }
}

