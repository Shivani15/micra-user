package com.relinns.micra.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.relinns.micra.R;

public class AboutusActivity extends AppCompatActivity {
LinearLayout backPressLLayout;
    TextView abtTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        abtTextView=(TextView)findViewById(R.id.abt_TV);
        Bundle extra=getIntent().getExtras();
        if(extra!=null)
        {
            String s1=getIntent().getStringExtra("about");
            if(s1.equals("Terms of use"))
            {
                abtTextView.setText(s1);
            }
            else if(s1.equals("Privacy Policy"))
            {
                abtTextView.setText(s1);

            }
        }
        else
        {

        }
        backPressLLayout=(LinearLayout)findViewById(R.id.backpress_LL);
        backPressLLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
