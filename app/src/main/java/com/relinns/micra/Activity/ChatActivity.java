package com.relinns.micra.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.relinns.micra.R;

public class ChatActivity extends AppCompatActivity {
    LinearLayout backPressLL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        backPressLL=(LinearLayout)findViewById(R.id.backPress);

        backPressLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            finish();

            }
        });


    }
}
