package com.relinns.micra.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra.Adapter.PermissionListAdapter;
import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 04-07-2017.
 */
public class PermissionListActivity extends Activity implements View.OnClickListener {

    ListView permissionLV;
    PermissionListAdapter permissionList_adapter;
    RelativeLayout backRL;
    TextView addnewTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissionlist);
        permissionLV=(ListView)findViewById(R.id.permissionLV);
        addnewTV=(TextView)findViewById(R.id.addnewTV);
        backRL=(RelativeLayout)findViewById(R.id.backRL);

        backRL.setOnClickListener(this);
        addnewTV.setOnClickListener(this);

        permissionList_adapter=new PermissionListAdapter(PermissionListActivity.this);
        permissionLV.setAdapter(permissionList_adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;
            case R.id.addnewTV:
                Intent intent= new Intent(PermissionListActivity.this,AddPermissionsActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exit);
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        PermissionListActivity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
