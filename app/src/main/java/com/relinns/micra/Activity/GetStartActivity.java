package com.relinns.micra.Activity;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.relinns.micra.Adapter.CustomSwipAdapter;
import com.relinns.micra.R;
import com.relinns.micra.model.ImageModel;

import java.util.ArrayList;

public class GetStartActivity extends AppCompatActivity {

    ViewPager viewPager;
    CustomSwipAdapter adapter;
    ArrayList<ImageModel> imageurl;
    ImageView image1IV,image2IV,image3IV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_start_);
        viewPager = (ViewPager)findViewById(R.id.viewpager1);

        image1IV = (ImageView)findViewById(R.id.image1);
        image2IV = (ImageView)findViewById(R.id.image2);
        image3IV = (ImageView)findViewById(R.id.image3);

        imageurl = new ArrayList<>();
        imageurl.add(new ImageModel(R.drawable.get_started));
        imageurl.add(new ImageModel(R.drawable.splash_logo));
        imageurl.add(new ImageModel(R.drawable.get_started));

        image1IV.setImageResource(R.drawable.circle_large);
        image2IV.setImageResource(R.drawable.circle_small);
        image3IV.setImageResource(R.drawable.circle_small);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                switch (position){

                    case 0:
                        image1IV.setImageResource(R.drawable.circle_large);
                        image2IV.setImageResource(R.drawable.circle_small);
                        image3IV.setImageResource(R.drawable.circle_small);
                        break;

                    case 1:
                        image1IV.setImageResource(R.drawable.circle_small);
                        image2IV.setImageResource(R.drawable.circle_large);
                        image3IV.setImageResource(R.drawable.circle_small);
                        break;

                    case 2:
                        image1IV.setImageResource(R.drawable.circle_small);
                        image2IV.setImageResource(R.drawable.circle_small);
                        image3IV.setImageResource(R.drawable.circle_large);
                        break;

                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        adapter = new CustomSwipAdapter(GetStartActivity.this,imageurl);
        viewPager.setAdapter(adapter);
    }

    public void login_open(View view){

        Intent intent = new Intent(GetStartActivity.this,LoginActivity.class);
        startActivity(intent);

    }

    public void open_signup(View view){

        Intent intent = new Intent(GetStartActivity.this,SignupActivity.class);
        startActivity(intent);

    }

    @Override
    public void onPause() {

        if(adapter != null)
        {
            adapter.notifyDataSetChanged();
        }
        super.onPause();
    }
}
