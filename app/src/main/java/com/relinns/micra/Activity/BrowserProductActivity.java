package com.relinns.micra.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;

import com.relinns.micra.Adapter.SearchProductAdapter;
import com.relinns.micra.R;
import com.relinns.micra.model.SearchProductModel;

import java.util.ArrayList;

public class BrowserProductActivity extends AppCompatActivity implements View.OnClickListener {
    Spinner s1;
    String[] s = {"All Category", "a", "b"};
    ArrayAdapter adapter;
    ListView lv;
    ImageView backpressImageView, cartText1ImageView;
    ArrayList<SearchProductModel> arraylist1;
    SearchProductAdapter spa_adapter;
    SearchProductModel spm_model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser__product);
        s1 = (Spinner) findViewById(R.id.spiner1);
        adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, s);
        s1.setAdapter(adapter);
        lv = (ListView) findViewById(R.id.list1);
        backpressImageView = (ImageView) findViewById(R.id.backPress);
        cartText1ImageView = (ImageView) findViewById(R.id.cartText1);
        backpressImageView.setOnClickListener(this);
        cartText1ImageView.setOnClickListener(this);

        arraylist1 = new ArrayList<>();
        arraylist1.add(new SearchProductModel("Product Name", "Price", R.drawable.winter));
        arraylist1.add(new SearchProductModel("Product Name", "Price", R.drawable.winter));
        arraylist1.add(new SearchProductModel("Product Name", "Price", R.drawable.winter));
        arraylist1.add(new SearchProductModel("Product Name", "Price", R.drawable.winter));

        //  spa_adapter=new SearchProductAdapter(arraylist1,SearchProductActivity.this);
        lv.setAdapter(spa_adapter);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backPress:
                finish();
                break;
            case R.id.cartText1:
                Intent i = new Intent(BrowserProductActivity.this, CartActivity.class);
                startActivity(i);
                break;
        }
    }
}
