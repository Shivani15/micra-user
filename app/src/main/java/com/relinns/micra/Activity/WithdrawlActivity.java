package com.relinns.micra.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RelativeLayout;

import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 07-07-2017.
 */

public class WithdrawlActivity extends Activity implements View.OnClickListener{
    RelativeLayout backRL;
    RelativeLayout withd1RL;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdrawl);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        withd1RL=(RelativeLayout)findViewById(R.id.withdrawlbutton);

        backRL.setOnClickListener(this);
        withd1RL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        WithdrawlActivity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}