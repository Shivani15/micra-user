package com.relinns.micra.Activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.relinns.micra.Fragment.BrowserVendorFragment;
import com.relinns.micra.Fragment.Maps1ListFragment;
import com.relinns.micra.R;

public class BrowserVendorActivity extends AppCompatActivity implements View.OnClickListener {
    FrameLayout fm;
    TextView Maps1TV, Vendors1TV;
    ImageView backpressIV, cartText1IV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser__vendor);
        fm = (FrameLayout) findViewById(R.id.frame_vendor1);
        Maps1TV = (TextView) findViewById(R.id.maps1);
        Vendors1TV = (TextView) findViewById(R.id.vendors1);
        backpressIV = (ImageView) findViewById(R.id.backPress);
        cartText1IV = (ImageView) findViewById(R.id.cartText1);
        cartText1IV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(BrowserVendorActivity.this, CartActivity.class);
                startActivity(i);
            }
        });
        backpressIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Maps1TV.setOnClickListener(this);
        Vendors1TV.setOnClickListener(this);
        Fragment fragment1 = new BrowserVendorFragment();
        FragmentTransaction fragmentTransaction1 = getSupportFragmentManager().beginTransaction();
        fragmentTransaction1.replace(R.id.frame_vendor1, fragment1, null);
        fragmentTransaction1.commit();


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.maps1:
                Maps1TV.setTextColor(getResources().getColor(R.color.white));
                Maps1TV.setBackgroundColor(getResources().getColor(R.color.micra));
                Vendors1TV.setTextColor(getResources().getColor(R.color.black));
                Vendors1TV.setBackgroundColor(getResources().getColor(R.color.white));

                Fragment fragment = new Maps1ListFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame_vendor1, fragment, null);
                fragmentTransaction.commit();

                break;
            case R.id.vendors1:
                Vendors1TV.setTextColor(getResources().getColor(R.color.white));
                Vendors1TV.setBackgroundColor(getResources().getColor(R.color.micra));
                Maps1TV.setTextColor(getResources().getColor(R.color.black));
                Maps1TV.setBackgroundColor(getResources().getColor(R.color.white));

                Fragment fragment1 = new BrowserVendorFragment();
                FragmentTransaction fragmentTransaction1 = getSupportFragmentManager().beginTransaction();
                fragmentTransaction1.replace(R.id.frame_vendor1, fragment1, null);
                fragmentTransaction1.commit();

                break;
        }
    }
}
