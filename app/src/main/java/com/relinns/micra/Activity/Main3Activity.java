package com.relinns.micra.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.relinns.micra.Fragment.Fragment3;
import com.relinns.micra.R;

public class Main3Activity extends AppCompatActivity {
     TextView b1TV,b2TV,chattext3TV;
    ImageView backPressIV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
         b1TV = (TextView) findViewById(R.id.about1);
         b2TV = (TextView) findViewById(R.id.frend1);
        chattext3TV = (TextView) findViewById(R.id.chattext1);



        chattext3TV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Main3Activity.this,ChatActivity.class);
                startActivity(i);
            }
        });

        backPressIV=(ImageView)findViewById(R.id.backPress);
        backPressIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Fragment hello = new Fragment3();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frag3, hello, null);
        fragmentTransaction.commit();
        b1TV.setBackgroundColor(getResources().getColor(R.color.micra));
        b1TV.setTextColor(getResources().getColor(R.color.white));
        b2TV.setTextColor(getResources().getColor(R.color.black));
        b2TV.setBackgroundColor(getResources().getColor(R.color.white));

        b1TV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment hello = new Fragment3();
                FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frag3, hello,null);
                fragmentTransaction.commit();
                b1TV.setBackgroundColor(getResources().getColor(R.color.micra));
                b1TV.setTextColor(getResources().getColor(R.color.white));
                b2TV.setTextColor(getResources().getColor(R.color.black));
                b2TV.setBackgroundColor(getResources().getColor(R.color.white));


            }
        });
        b2TV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment hello = new Fragment3();
                FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frag3, hello,null);
                fragmentTransaction.commit();
                b2TV.setBackgroundColor(getResources().getColor(R.color.micra));
                b2TV.setTextColor(getResources().getColor(R.color.white));
                b1TV.setTextColor(getResources().getColor(R.color.black));
                b1TV.setBackgroundColor(getResources().getColor(R.color.white));


            }
        });}




}
