package com.relinns.micra.Activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.relinns.micra.Fragment.Fragment2;
import com.relinns.micra.R;

public class Main2Activity extends AppCompatActivity {
   TextView b1TV,b2TV,b3TV;
    ImageView backPressIV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        backPressIV=(ImageView)findViewById(R.id.backPress);

        b1TV=(TextView)findViewById(R.id.about1);
        b2TV=(TextView)findViewById(R.id.frend1);
        b3TV=(TextView)findViewById(R.id.request1);
        backPressIV=(ImageView)findViewById(R.id.backPress);
        backPressIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        android.support.v4.app.Fragment hello = new Fragment2();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frag2, hello,null);
        fragmentTransaction.commit();
        b1TV.setBackgroundColor(getResources().getColor(R.color.micra));
        b1TV.setTextColor(getResources().getColor(R.color.white));
        b2TV.setTextColor(getResources().getColor(R.color.black));
        b2TV.setBackgroundColor(getResources().getColor(R.color.white));
        b3TV.setTextColor(getResources().getColor(R.color.black));
        b3TV.setBackgroundColor(getResources().getColor(R.color.white));
        b1TV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v4.app.Fragment hello = new Fragment2();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frag2, hello,null);
                fragmentTransaction.commit();
                b1TV.setBackgroundColor(getResources().getColor(R.color.micra));
                b1TV.setTextColor(getResources().getColor(R.color.white));
                b2TV.setTextColor(getResources().getColor(R.color.black));
                b2TV.setBackgroundColor(getResources().getColor(R.color.white));
                b3TV.setTextColor(getResources().getColor(R.color.black));
                b3TV.setBackgroundColor(getResources().getColor(R.color.white));

            }
        });
        b2TV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v4.app.Fragment hello = new Fragment2();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frag2, hello,null);
                fragmentTransaction.commit();
                b2TV.setBackgroundColor(getResources().getColor(R.color.micra));
                b2TV.setTextColor(getResources().getColor(R.color.white));
                b1TV.setTextColor(getResources().getColor(R.color.black));
                b1TV.setBackgroundColor(getResources().getColor(R.color.white));
                b3TV.setTextColor(getResources().getColor(R.color.black));
                b3TV.setBackgroundColor(getResources().getColor(R.color.white));

            }
        });
        b3TV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v4.app.Fragment hello = new Fragment2();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frag2, hello,null);
                fragmentTransaction.commit();
                b3TV.setBackgroundColor(getResources().getColor(R.color.micra));
                b3TV.setTextColor(getResources().getColor(R.color.white));
                b2TV.setTextColor(getResources().getColor(R.color.black));
                b2TV.setBackgroundColor(getResources().getColor(R.color.white));
                b1TV.setTextColor(getResources().getColor(R.color.black));
                b1TV.setBackgroundColor(getResources().getColor(R.color.white));

            }
        });
    }}
