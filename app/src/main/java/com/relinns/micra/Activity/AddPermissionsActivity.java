package com.relinns.micra.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 05-07-2017.
 */
public class AddPermissionsActivity extends Activity  implements View.OnClickListener {

    RelativeLayout backRL;
    TextView vewpermisionTV;
    Button checkdetailBTN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addpermissions);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        vewpermisionTV=(TextView)findViewById(R.id.vewpermisionTV);
        checkdetailBTN=(Button)findViewById(R.id.checkdetail_BT);
        backRL.setOnClickListener(this);
        checkdetailBTN.setOnClickListener(this);
        vewpermisionTV.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;
            case R.id.vewpermisionTV:
                onBackPressed();

                break;

            case R.id.checkdetail_BT:

                Intent intent=new Intent(AddPermissionsActivity.this,EmployDetailActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exit);


                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AddPermissionsActivity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
