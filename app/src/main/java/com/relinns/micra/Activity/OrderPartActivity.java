package com.relinns.micra.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.relinns.micra.R;

public class OrderPartActivity extends AppCompatActivity {
    TextView AskForRefundTV;
    ImageView backPressIV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order__part4);
        AskForRefundTV= (TextView) findViewById(R.id.askforrefund1);
        backPressIV = (ImageView) findViewById(R.id.backPress);
        backPressIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        AskForRefundTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OrderPartActivity.this, Main4Activity.class);
                startActivity(i);
            }
        });
    }
}
