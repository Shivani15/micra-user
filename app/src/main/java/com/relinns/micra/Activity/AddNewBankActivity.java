package com.relinns.micra.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RelativeLayout;

import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 06-07-2017.
 */

public class AddNewBankActivity extends Activity  implements View.OnClickListener{
    RelativeLayout bankRLayout,backRLayout;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addnewbank);
        backRLayout=(RelativeLayout)findViewById(R.id.back_RL);
        bankRLayout=(RelativeLayout)findViewById(R.id.bank_RL);

        backRLayout.setOnClickListener(this);
        bankRLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i=new Intent(AddNewBankActivity.this,EditBankActivity.class);
//                startActivity(i);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back_RL:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        AddNewBankActivity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
