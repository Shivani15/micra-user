package com.relinns.micra.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.relinns.micra.R;

public class UploadPlaceOrderActivity extends AppCompatActivity {
LinearLayout lineLL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_place_order);
        lineLL=(LinearLayout)findViewById(R.id.place2);

    }
    public void backpress(View v)
    {
        finish();
    }
    public void place_order(View v)
    {
        Intent i= new Intent(UploadPlaceOrderActivity.this,OrderPlacedActivity.class);
        startActivity(i);
    }
}
