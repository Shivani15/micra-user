package com.relinns.micra.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RelativeLayout;

import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 06-07-2017.
 */

public class EditCardActivity extends Activity implements View.OnClickListener {

    RelativeLayout backRL;
    RelativeLayout editCardLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_editcard);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        editCardLayout=(RelativeLayout)findViewById(R.id.editcard1);

        backRL.setOnClickListener(this);
        editCardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i=new Intent(EditCardActivity.this,Chat.class);
//                startActivity(i);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:

                onBackPressed();

                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        EditCardActivity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
