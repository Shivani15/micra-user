package com.relinns.micra.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.relinns.micra.R;

public class LoginActivity extends AppCompatActivity {
LinearLayout logi1LL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_);
        logi1LL=(LinearLayout)findViewById(R.id.logi1);
        logi1LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(LoginActivity.this,MenuListActivity.class);
                i.putExtra("hlo1","1");
                startActivity(i);
            }
        });
    }

    public void open_reg(View view){

        Intent intent = new Intent(LoginActivity.this,SignupActivity.class);
        startActivity(intent);

    }

    public void open_forgot(View view){

        Intent intent = new Intent(LoginActivity.this,ForgotActivity.class);
        startActivity(intent);

    }
    public void login(View view){

        Intent intent = new Intent(LoginActivity.this,ForgotActivity.class);
        startActivity(intent);

    }


    public void backpress(View view){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
