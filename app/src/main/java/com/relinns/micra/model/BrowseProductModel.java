package com.relinns.micra.model;

/**
 * Created by Relinns on 20-07-2017.
 */

public class BrowseProductModel {
    String price,product;
    Integer image;

    public BrowseProductModel(String product, String price, Integer image) {
        this.product = product;
        this.price = price;
        this.image = image;
    }


    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

}
