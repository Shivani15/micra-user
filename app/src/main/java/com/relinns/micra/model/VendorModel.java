package com.relinns.micra.model;

/**
 * Created by knl on 7/11/2017.
 */

public class VendorModel {
    String name, payment, distance;
    Integer image;
    public VendorModel(Integer image, String name, String distance, String payment)
    {
        this.image=image;
        this.name=name;
        this.distance=distance;
        this.payment=payment;
    }
    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }




}
