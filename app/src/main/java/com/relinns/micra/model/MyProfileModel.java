package com.relinns.micra.model;

/**
 * Created by knl on 7/14/2017.
 */

public class MyProfileModel {
    String Product;
    String Price;
    Integer image;

    public MyProfileModel(String product, String price, Integer image) {
        Product = product;
        Price = price;

        this.image = image;
    }

    public String getProduct() {
        return Product;
    }

    public void setProduct(String product)
    {
        Product = product;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }


}
