package com.relinns.micra.model;

/**
 * Created by admin on 06-07-2017.
 */
public class CartModel {
    Integer place_holder;
    String product_name,  vendor_name,  price,  offer,  quantity;
    public CartModel(Integer place_holder, String product_name, String vendor_name, String price, String offer, String quantity) {
        this.place_holder=place_holder;
        this.price=price;
        this.product_name=product_name;
        this.vendor_name=vendor_name;
        this.offer=offer;
        this.quantity=quantity;
    }

    public Integer getPlace_holder() {
        return place_holder;
    }

    public void setPlace_holder(Integer place_holder) {
        this.place_holder = place_holder;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getVendor_name() {
        return vendor_name;
    }

    public void setVendor_name(String vendor_name) {
        this.vendor_name = vendor_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
