package com.relinns.micra.model;

/**
 * Created by knl on 7/14/2017.
 */

public class MessageModel
{
    Integer offline_user;
    String user_name,  place;
    public MessageModel(Integer offline_user, String user_name, String place) {
        this.offline_user=offline_user;
        this.user_name=user_name;
        this.place=place;
    }

    public Integer getOffline_user() {
        return offline_user;
    }

    public void setOffline_user(Integer offline_user) {
        this.offline_user = offline_user;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
}



