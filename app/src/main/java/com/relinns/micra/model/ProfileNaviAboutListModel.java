package com.relinns.micra.model;

/**
 * Created by Relinns on 28-07-2017.
 */

public class ProfileNaviAboutListModel {

    int image1;
    String John, mohali;

    public int getImage1() {
        return image1;
    }

    public void setImage1(int image1) {
        this.image1 = image1;
    }

    public String getJohn() {
        return John;
    }

    public void setJohn(String john) {
        John = john;
    }

    public String getMohali() {
        return mohali;
    }

    public void setMohali(String mohali) {
        this.mohali = mohali;
    }

    public ProfileNaviAboutListModel(int image1, String john, String mohali) {
        this.image1 = image1;
        this.John = john;

        this.mohali = mohali;
    }
}


