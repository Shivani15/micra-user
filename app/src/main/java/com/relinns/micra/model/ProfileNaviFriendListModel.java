package com.relinns.micra.model;

import android.widget.ImageView;

/**
 * Created by Relinns on 28-07-2017.
 */

public class ProfileNaviFriendListModel {

    int image1;
    String John, mohali;

    public int getImage1() {
        return image1;
    }

    public void setImage1(int image1) {
        this.image1 = image1;
    }

    public String getJohn() {
        return John;
    }

    public void setJohn(String john) {
        John = john;
    }

    public String getMohali() {
        return mohali;
    }

    public void setMohali(String mohali) {
        this.mohali = mohali;
    }

    public ProfileNaviFriendListModel(int image1, String john, String mohali) {
        this.image1 = image1;
        John = john;

        this.mohali = mohali;
    }
}
