package com.relinns.micra.model;


/**
 * Created by Relinns on 19-07-2017.
 */

public class SearchProductModel {
    String product;

    public SearchProductModel(String product, String price, Integer image) {
        this.product = product;
        this.price = price;
        this.image = image;
    }

    String price;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    Integer image;
}
