package com.relinns.micra.model;

/**
 * Created by admin on 06-07-2017.
 */
public class CustomerModel {
    Integer offline_user;
    String user_name,  place,  order;
    public CustomerModel(Integer offline_user, String user_name, String place, String order) {
        this.offline_user=offline_user;
        this.user_name=user_name;
        this.place=place;
        this.order=order;
    }

    public Integer getOffline_user() {
        return offline_user;
    }

    public void setOffline_user(Integer offline_user) {
        this.offline_user = offline_user;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
}
