package com.relinns.micra.model;

/**
 * Created by Relinns on 20-07-2017.
 */

public class RetrailerListModel {

    String name, payment, distance;
    Integer image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public RetrailerListModel(String name, String payment, String distance, Integer image) {
        this.name = name;
        this.payment = payment;

        this.distance = distance;
        this.image = image;
    }
}
