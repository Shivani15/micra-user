package com.relinns.micra.model;

/**
 * Created by knl on 7/12/2017.
 */

public class VendorProfileModel {

    String Product;
    String Price;
    Integer image;
    public VendorProfileModel(String Product, String Price, Integer image) {
        this.Product=Product;
        this.Price=Price;
        this.image=image;
    }

    public String getProduct() {
        return Product;
    }

    public void setProduct(String product) {
        Product = product;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }
}
