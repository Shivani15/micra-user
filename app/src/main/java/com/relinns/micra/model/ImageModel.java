package com.relinns.micra.model;

/**
 * Created by Relinns-Technologies on 3/23/2017.
 */

public class ImageModel {

    int url;

    public ImageModel(int url) {
        this.url = url;
    }

    public int getUrl() {
        return url;
    }

    public void setUrl(int url) {
        this.url = url;
    }
}
