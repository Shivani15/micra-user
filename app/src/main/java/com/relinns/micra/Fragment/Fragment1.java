package com.relinns.micra.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.relinns.micra.R;

/**
 * Created by knl on 7/6/2017.
 */

public class Fragment1 extends Fragment {
   Button addasFreindBtn;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragments1,container,false);
        addasFreindBtn=(Button)v.findViewById(R.id.AddAs);
        addasFreindBtn.setOnClickListener(new View.OnClickListener() {
          @Override
            public void onClick(View v) {
            }
       });
                return v;
    }
}
