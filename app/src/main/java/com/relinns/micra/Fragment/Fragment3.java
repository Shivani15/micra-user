package com.relinns.micra.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.relinns.micra.Activity.TrackOrder2Activity;
import com.relinns.micra.R;

/**
 * Created by knl on 7/7/2017.
 */

public class Fragment3 extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment3,container,false);
        Button trackingBtn=(Button)v.findViewById(R.id.track1);
        trackingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(),TrackOrder2Activity.class);
                i.putExtra("friend","Track Friend");
                startActivity(i);

            }
        });
        return v;

    }
}
