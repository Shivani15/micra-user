package com.relinns.micra.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.relinns.micra.Activity.AddAddressActivity;
import com.relinns.micra.Activity.EditAddressActivity;
import com.relinns.micra.Activity.MenuListActivity;
import com.relinns.micra.Adapter.ProfileNaviSideAboutListAdapter;
import com.relinns.micra.Adapter.ProfileNaviSideFriendListAdapter;
import com.relinns.micra.Adapter.ProfileNaviSideRequestListAdapter;
import com.relinns.micra.R;
import com.relinns.micra.model.ProfileNaviAboutListModel;
import com.relinns.micra.model.ProfileNaviFriendListModel;
import com.relinns.micra.model.ProfileNaviRequestListModel;

import java.util.ArrayList;

/**
 * Created by Relinns on 26-07-2017.
 */

public class ProfileNavigationSideWay extends Fragment implements View.OnClickListener {
    TextView about1TV, friendTV, requestTV, addNewTV;
    LinearLayout friendLinlayout, RequestLinlayout;
    ScrollView aboutScrollView;
    Button btn1;
    ProfileNaviSideFriendListAdapter adapter_frend;
    ProfileNaviSideAboutListAdapter adapter_about;
    ProfileNaviSideRequestListAdapter adapter_request;

    ListView aboutLV, friendLV, requestLV;
    ArrayList<ProfileNaviFriendListModel> arrayList_frend;
    ArrayList<ProfileNaviRequestListModel> arrayList_request;
    ArrayList<ProfileNaviAboutListModel> arrayList_about;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.profile_navigation_side_way, container, false);
        about1TV = (TextView) v.findViewById(R.id.about1);
        friendTV = (TextView) v.findViewById(R.id.friend1);
        requestTV = (TextView) v.findViewById(R.id.request1);
        friendLinlayout = (LinearLayout) v.findViewById(R.id.friend_layout);
        RequestLinlayout = (LinearLayout) v.findViewById(R.id.Request_layout);
        addNewTV = (TextView) v.findViewById(R.id.add_new);
        btn1 = (Button) v.findViewById(R.id.edit1);
        aboutScrollView = (ScrollView) v.findViewById(R.id.about_layout);
        friendLV = (ListView) v.findViewById(R.id.list_friend);
        requestLV = (ListView) v.findViewById(R.id.list_request);
        aboutLV = (ListView) v.findViewById(R.id.list_about);

        ((MenuListActivity) getActivity()).settitleactivity("My Profile");

        about1TV.setOnClickListener(this);
        friendTV.setOnClickListener(this);
        requestTV.setOnClickListener(this);
        addNewTV.setOnClickListener(this);
        btn1.setOnClickListener(this);


        about1TV.setBackgroundColor(getResources().getColor(R.color.micra));
        about1TV.setTextColor(getResources().getColor(R.color.white));
        aboutScrollView.setVisibility(View.VISIBLE);
        friendLinlayout.setVisibility(View.GONE);
        RequestLinlayout.setVisibility(View.GONE);

        arrayList_about = new ArrayList<ProfileNaviAboutListModel>();

        arrayList_about.add(new ProfileNaviAboutListModel(R.drawable.insaan2, "John Doe", "Loremipsum dolor sit amet,consecetur tanti aestimarels.uae uidem"));
        arrayList_about.add(new ProfileNaviAboutListModel(R.drawable.insaan2, "John Doe", "Lorem ipsum dolor sit amet,consecetur tanti aestimarels.uae uidem"));
        arrayList_about.add(new ProfileNaviAboutListModel(R.drawable.insaan2, "John Doe", "Lorem ipsum dolor sit amet,consecetur tanti aestimarels.uae uidem"));
        arrayList_about.add(new ProfileNaviAboutListModel(R.drawable.insaan2, "John Doe", "Lorem ipsum dolor sit amet,consecetur tanti aestimarels.uae uidem"));
        arrayList_about.add(new ProfileNaviAboutListModel(R.drawable.insaan2, "John Doe", "Loremipsum dolor sit amet,consecetur tanti aestimarels.uae uidem"));


        adapter_about = new ProfileNaviSideAboutListAdapter(getActivity(), arrayList_about);
        aboutLV.setAdapter(adapter_about);
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.about1:
                aboutScrollView.setVisibility(View.VISIBLE);
                friendLinlayout.setVisibility(View.GONE);
                RequestLinlayout.setVisibility(View.GONE);

                about1TV.setBackgroundColor(getResources().getColor(R.color.micra));
                about1TV.setTextColor(getResources().getColor(R.color.white));
                friendTV.setBackgroundColor(getResources().getColor(R.color.white));
                friendTV.setTextColor(getResources().getColor(R.color.black));
                requestTV.setBackgroundColor(getResources().getColor(R.color.white));
                requestTV.setTextColor(getResources().getColor(R.color.black));

                arrayList_about = new ArrayList<ProfileNaviAboutListModel>();

                arrayList_about.add(new ProfileNaviAboutListModel(R.drawable.insaan2, "John Doe", "Loremipsum dolor sit amet,consecetur tanti aestimarels.uae uidem"));
                arrayList_about.add(new ProfileNaviAboutListModel(R.drawable.insaan2, "John Doe", "Loremipsum dolor sit amet,consecetur tanti aestimarels.uae uidem"));
                arrayList_about.add(new ProfileNaviAboutListModel(R.drawable.insaan2, "John Doe", "Lorem ipsum dolor sit amet,consecetur tanti aestimarels.uae uidem"));
                arrayList_about.add(new ProfileNaviAboutListModel(R.drawable.insaan2, "John Doe", "Lorem ipsum dolor sit amet,consecetur tanti aestimarels.uae uidem"));
                arrayList_about.add(new ProfileNaviAboutListModel(R.drawable.insaan2, "John Doe", "Lorem ipsum dolor sit amet,consecetur tanti aestimarels.uae uidem"));
                adapter_about = new ProfileNaviSideAboutListAdapter(getActivity(), arrayList_about);
                aboutLV.setAdapter(adapter_about);
                break;

            case R.id.friend1:
                friendLinlayout.setVisibility(View.VISIBLE);
                aboutScrollView.setVisibility(View.GONE);
                RequestLinlayout.setVisibility(View.GONE);

                friendTV.setBackgroundColor(getResources().getColor(R.color.micra));
                friendTV.setTextColor(getResources().getColor(R.color.white));
                about1TV.setBackgroundColor(getResources().getColor(R.color.white));
                about1TV.setTextColor(getResources().getColor(R.color.black));
                requestTV.setBackgroundColor(getResources().getColor(R.color.white));
                requestTV.setTextColor(getResources().getColor(R.color.black));
                arrayList_frend = new ArrayList<ProfileNaviFriendListModel>();
                arrayList_frend.add(new ProfileNaviFriendListModel(R.drawable.offline_user1, "John Doe", "Mohali,India"));
                arrayList_frend.add(new ProfileNaviFriendListModel(R.drawable.offline_user1, "John Doe", "Mohali,India"));
                arrayList_frend.add(new ProfileNaviFriendListModel(R.drawable.offline_user1, "John Doe", "Mohali,India"));
                arrayList_frend.add(new ProfileNaviFriendListModel(R.drawable.offline_user1, "John Doe", "Mohali,India"));
                arrayList_frend.add(new ProfileNaviFriendListModel(R.drawable.offline_user1, "John Doe", "Mohali,India"));
                adapter_frend = new ProfileNaviSideFriendListAdapter(getActivity(), arrayList_frend);
                friendLV.setAdapter(adapter_frend);
                break;

            case R.id.request1:
                RequestLinlayout.setVisibility(View.VISIBLE);
                aboutScrollView.setVisibility(View.GONE);
                friendLinlayout.setVisibility(View.GONE);

                requestTV.setBackgroundColor(getResources().getColor(R.color.micra));
                requestTV.setTextColor(getResources().getColor(R.color.white));
                friendTV.setBackgroundColor(getResources().getColor(R.color.white));
                friendTV.setTextColor(getResources().getColor(R.color.black));
                about1TV.setBackgroundColor(getResources().getColor(R.color.white));
                about1TV.setTextColor(getResources().getColor(R.color.black));
                arrayList_request = new ArrayList<ProfileNaviRequestListModel>();
                arrayList_request.add(new ProfileNaviRequestListModel(R.drawable.insaan3, "John Doe", "Mohali,India"));
                arrayList_request.add(new ProfileNaviRequestListModel(R.drawable.insaan3, "John Doe", "Mohali,India"));
                arrayList_request.add(new ProfileNaviRequestListModel(R.drawable.insaan3, "John Doe", "Mohali,India"));
                arrayList_request.add(new ProfileNaviRequestListModel(R.drawable.insaan3, "John Doe", "Mohali,India"));
                arrayList_request.add(new ProfileNaviRequestListModel(R.drawable.insaan3, "John Doe", "Mohali,India"));
                adapter_request = new ProfileNaviSideRequestListAdapter(getActivity(), arrayList_request);
                requestLV.setAdapter(adapter_request);
                break;

            case R.id.add_new:
                Intent i1 = new Intent(getActivity(), AddAddressActivity.class);
                startActivity(i1);
                break;
            case R.id.edit1:
                Intent i2 = new Intent(getActivity(), EditAddressActivity.class);
                startActivity(i2);
                break;
        }
    }
}
