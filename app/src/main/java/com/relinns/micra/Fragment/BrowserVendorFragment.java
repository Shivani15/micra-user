
package com.relinns.micra.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.relinns.micra.Adapter.BrowserVendorAdapter;
import com.relinns.micra.R;
import com.relinns.micra.model.BrowserVendorModel;

import java.util.ArrayList;

/**
 * Created by Relinns on 20-07-2017.
 */

public class BrowserVendorFragment extends Fragment {
    BrowserVendorAdapter adapter;
    ListView lv;
    ArrayList<BrowserVendorModel> arrayList;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.browser_vendor_fragment,container,false);
        lv=(ListView)v.findViewById(R.id.list1);
        arrayList=new ArrayList<BrowserVendorModel>();
        arrayList.add(new BrowserVendorModel("Sai Medical Store","Payment:Mode,Code Wallet","3 km away",R.drawable.face1));
        arrayList.add(new BrowserVendorModel("Sai Medical Store","Payment:Mode,Code Wallet","3 km away",R.drawable.face1));
        arrayList.add(new BrowserVendorModel("Sai Medical Store","Payment:Mode,Code Wallet","3 km away",R.drawable.face1));
        arrayList.add(new BrowserVendorModel("Sai Medical Store","Payment:Mode,Code Wallet","3 km away",R.drawable.face1));
        adapter=new BrowserVendorAdapter(getActivity(),arrayList);
        lv.setAdapter(adapter);
        return v;
    }
}
