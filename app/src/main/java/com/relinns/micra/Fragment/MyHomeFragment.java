package com.relinns.micra.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.relinns.micra.Activity.BrowseProductActivity;
import com.relinns.micra.Activity.BrowserVendorActivity;
import com.relinns.micra.Activity.Main3Activity;
import com.relinns.micra.Activity.MenuListActivity;
import com.relinns.micra.Activity.SearchProductActivity;
import com.relinns.micra.Activity.SearchUserActivity;
import com.relinns.micra.Activity.SearchVendorActivity;
import com.relinns.micra.R;

/**
 * Created by Relinns on 18-07-2017.
 */

public class MyHomeFragment extends Fragment implements View.OnClickListener {
    RelativeLayout r1,r2,r3,r4,r9,rel1,rel2,rel3,rel4;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_home1, container, false);
        ((MenuListActivity)getActivity()).settitleactivity("Micra");

        r1 = (RelativeLayout) v.findViewById(R.id.search_product_rel1);
        r2 = (RelativeLayout) v.findViewById(R.id.search_product_rel2);
        r3 = (RelativeLayout) v.findViewById(R.id.search_product_rel3);
        r4 = (RelativeLayout) v.findViewById(R.id.search_product_rel4);
        r9 = (RelativeLayout) v.findViewById(R.id.ser_user9);
        rel1 = (RelativeLayout) v.findViewById(R.id.rel1);
        rel2 = (RelativeLayout) v.findViewById(R.id.rel2);
        rel3 = (RelativeLayout) v.findViewById(R.id.rel3);
        rel4 = (RelativeLayout) v.findViewById(R.id.rel4);

        r1.setOnClickListener(this);
        r2.setOnClickListener(this);
        r3.setOnClickListener(this);
        r4.setOnClickListener(this);
        r9.setOnClickListener(this);
        rel1.setOnClickListener(this);
        rel2.setOnClickListener(this);
        rel3.setOnClickListener(this);
        rel4.setOnClickListener(this);

        return v;

    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.search_product_rel1:
                Intent i=new Intent(getActivity(),SearchProductActivity.class);
                startActivity(i);
                break;

            case R.id.search_product_rel2:
                Intent i1=new Intent(getActivity(),BrowseProductActivity.class);
                startActivity(i1);
                break;
            case R.id.search_product_rel3:
                Intent i2=new Intent(getActivity(),SearchVendorActivity.class);
                startActivity(i2);
                break;
            case R.id.search_product_rel4:
                Intent i3=new Intent(getActivity(),BrowserVendorActivity.class);
                startActivity(i3);
                break;
            case R.id.ser_user9:
                Intent i9=new Intent(getActivity(),SearchUserActivity.class);
                startActivity(i9);
                break;

            case R.id.rel1:
                Intent i10=new Intent(getActivity(),MenuListActivity.class);
                i10.putExtra("hlo1","3");
                startActivity(i10);
                break;
            case R.id.rel2:
                Intent i11=new Intent(getActivity(), Main3Activity.class);
                startActivity(i11);
                break;
            case R.id.rel3:
            Intent i12=new Intent(getActivity(),MenuListActivity.class);
                i12.putExtra("hlo1","4");

                startActivity(i12);
            break;
            case R.id.rel4:
            Intent i13=new Intent(getActivity(),MenuListActivity.class);
                i13.putExtra("hlo1","2");

                startActivity(i13);
            break;

        }

    }
    }

