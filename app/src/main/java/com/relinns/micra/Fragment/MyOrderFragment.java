package com.relinns.micra.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.relinns.micra.R;

/**
 * Created by Relinns on 18-07-2017.
 */

public class MyOrderFragment extends Fragment {
    LinearLayout linLayout;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.activity_main7,container,false);

        linLayout=(LinearLayout)v.findViewById(R.id.search_Frag1);

        linLayout.setVisibility(View.GONE);


        return  v;
    }
}
