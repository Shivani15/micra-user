package com.relinns.micra.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.relinns.micra.Activity.MenuListActivity;
import com.relinns.micra.Adapter.FavouriteBuyerAdapter;
import com.relinns.micra.Adapter.FavouriteProductAdapter;
import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 01-07-2017.
 */
public class MyFavouriteFragment extends Fragment implements View.OnClickListener {
    TextView favproductTV,favbuyerTV;
    ListView favproductLV,favbuyerLV;
    FavouriteBuyerAdapter favouriteBuyer_adapter;
    FavouriteProductAdapter favouriteProduct_adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.myfavourite_fragment,container,false);
        ((MenuListActivity) getActivity()).settitleactivity("Favourites");

        favbuyerTV=(TextView)view.findViewById(R.id.favbuyerTV);
        favproductTV=(TextView)view.findViewById(R.id.favproductTV);
        favproductLV=(ListView)view.findViewById(R.id.favproductLV);
        favbuyerLV=(ListView)view.findViewById(R.id.favbuyerLV);

        favbuyerTV.setOnClickListener(this);
        favproductTV.setOnClickListener(this);


        favouriteProduct_adapter=new FavouriteProductAdapter(getActivity());
        favproductLV.setAdapter(favouriteProduct_adapter);


        favouriteBuyer_adapter=new FavouriteBuyerAdapter(getActivity());
        favbuyerLV.setAdapter(favouriteBuyer_adapter);


        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.favproductTV:



                favproductLV.setVisibility(View.VISIBLE);
                favbuyerLV.setVisibility(View.GONE);

                favproductTV.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                favproductTV.setTextColor(getResources().getColor(R.color.white));

                favbuyerTV.setBackgroundColor(getResources().getColor(R.color.white));
                favbuyerTV.setTextColor(getResources().getColor(R.color.black));
                favproductTV.setPadding(10, 10, 10, 10);
                favbuyerTV.setPadding(10, 10, 10, 10);

                break;
            case R.id.favbuyerTV:

                favproductLV.setVisibility(View.GONE);
                favbuyerLV.setVisibility(View.VISIBLE);
                favbuyerTV.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                favbuyerTV.setTextColor(getResources().getColor(R.color.white));
                favproductTV.setBackgroundColor(getResources().getColor(R.color.white));
                favproductTV.setTextColor(getResources().getColor(R.color.black));
                favproductTV.setPadding(10,10,10,10);
                favbuyerTV.setPadding(10,10,10,10);

                break;
        }

    }
}
