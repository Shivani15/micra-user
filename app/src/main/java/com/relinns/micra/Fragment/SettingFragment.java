package com.relinns.micra.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.relinns.micra.Activity.AboutusActivity;
import com.relinns.micra.Activity.BlockedUserActivity;
import com.relinns.micra.Activity.MenuListActivity;
import com.relinns.micra.Activity.PermissionListActivity;
import com.relinns.micra.Activity.ReportFeedbackActivity;
import com.relinns.micra.R;

/**
 * Created by Relinns Technologies on 04-07-2017.
 */
public class SettingFragment extends Fragment implements View.OnClickListener {

    LinearLayout reportfeedLL,blockuser_LL,permissionLL,aboutUsLL,termsLL,policyLL;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.settingfragment,container,false);
        ((MenuListActivity)getActivity()).settitleactivity("Settings");

        reportfeedLL=(LinearLayout)view.findViewById(R.id.reportfeed_RL);
        blockuser_LL=(LinearLayout)view.findViewById(R.id.blockuser_LL);
        permissionLL=(LinearLayout)view.findViewById(R.id.permissionLL);

        aboutUsLL=(LinearLayout)view.findViewById(R.id.aboutUs);
        termsLL=(LinearLayout)view.findViewById(R.id.terms);
        policyLL=(LinearLayout)view.findViewById(R.id.policy);

        blockuser_LL.setOnClickListener(this);
        aboutUsLL.setOnClickListener(this);
        reportfeedLL.setOnClickListener(this);
        permissionLL.setOnClickListener(this);
        termsLL.setOnClickListener(this);
        policyLL.setOnClickListener(this);


        return view;
    }


    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.blockuser_LL:
                Intent i1=new Intent(getActivity(), BlockedUserActivity.class);
                startActivity(i1);

                break;
            case R.id.aboutUs:
                Intent i2=new Intent(getActivity(), AboutusActivity.class);
                startActivity(i2);

                break;
            case R.id.permissionLL:
                Intent i3=new Intent(getActivity(),PermissionListActivity.class);
                startActivity(i3);

                break;
            case R.id.reportfeed_RL:
                Intent i4=new Intent(getActivity(),ReportFeedbackActivity.class);
                startActivity(i4);

                break;
            case R.id.terms:
                        Intent i5=new Intent(getActivity(),AboutusActivity.class);
                i5.putExtra("about","Terms of use");
                startActivity(i5);

                break;
            case R.id.policy:
                Intent i6=new Intent(getActivity(),AboutusActivity.class);
                i6.putExtra("about","Privacy Policy");

                startActivity(i6);

                break;
        }
    }
}