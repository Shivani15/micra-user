package com.relinns.micra.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.relinns.micra.Adapter.VendorProfileAdapter;
import com.relinns.micra.R;
import com.relinns.micra.model.VendorProfileModel;

import java.util.ArrayList;

/**
 * Created by knl on 7/12/2017.
 */

public class ProfileVendorFragment extends Fragment {
    ListView lv;
    VendorProfileAdapter adapter;
    ArrayList<VendorProfileModel> list1;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.vendor_profile_fragment_parent,container,false);
        lv=(ListView)v.findViewById(R.id.list1);
        list1=new ArrayList<>();
        list1.add(new VendorProfileModel("Product Name","Price",R.drawable.screen1));
        list1.add(new VendorProfileModel("Product Name","Price",R.drawable.screen1));
        list1.add(new VendorProfileModel("Product Name","Price", R.drawable.screen1));
         adapter=new VendorProfileAdapter(getActivity(),list1);
        lv.setAdapter(adapter);
        return v;
    }
}
