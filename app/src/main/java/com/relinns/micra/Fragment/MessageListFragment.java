package com.relinns.micra.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.relinns.micra.Activity.MenuListActivity;
import com.relinns.micra.Adapter.MessageListAdapter;
import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 01-07-2017.
 */
public class MessageListFragment extends Fragment {
    ListView messageLV;
    MessageListAdapter messageList_adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.messagelistfragment,container,false);
        ((MenuListActivity) getActivity()).settitleactivity("Message");

        messageLV=(ListView)view.findViewById(R.id.messageLV);

        messageList_adapter =new MessageListAdapter(getActivity());
        messageLV.setAdapter(messageList_adapter);
        return view;
    }
}
