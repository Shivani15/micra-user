package com.relinns.micra.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 06-07-2017.
 */

public class OnlyProfileFragment extends Fragment implements View.OnClickListener{
    RelativeLayout addnewRL;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.onlyprofile_fragment,container,false);

        addnewRL=(RelativeLayout)view.findViewById(R.id.addnewRL);
        addnewRL.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addnewRL:
               /* Intent intent1=new Intent(getActivity(),AddProfile_Address_Activity.class);
                startActivity(intent1);
                getActivity().overridePendingTransition(R.anim.enter,R.anim.exit);*/
                break;
        }

    }
}
