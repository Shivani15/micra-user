package com.relinns.micra.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.relinns.micra.Activity.AddNewBankActivity;
import com.relinns.micra.Activity.AddNewCardActivity;
import com.relinns.micra.Activity.EditBankActivity;
import com.relinns.micra.Activity.EditCardActivity;
import com.relinns.micra.Activity.MenuListActivity;
import com.relinns.micra.Activity.SendMoneyActivity;
import com.relinns.micra.Activity.TopUpActivity;
import com.relinns.micra.Activity.WithdrawlActivity;
import com.relinns.micra.Adapter.WalletHistoryAdapter;
import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 06-07-2017.
 */

public class MyWalletFragment extends Fragment implements View.OnClickListener {

    LinearLayout overviewLL, historyLL, bankLL, over_LL, editCard2;
    TextView overviewTV, historyTV, bankTV;
    ListView history_LV, bank_LV;
    WalletHistoryAdapter walletHistory_adapter;
    ScrollView bankScrollView;
    RelativeLayout addnewbank_RL, addnewcardRL, topupRL, sendmoneyRL, requestRL, withdralRL;
    LinearLayout editbankTV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mywallet_fragment, container, false);
        ((MenuListActivity) getActivity()).settitleactivity("Wallet");
        overviewLL = (LinearLayout) view.findViewById(R.id.overviewLL);
        over_LL = (LinearLayout) view.findViewById(R.id.over_LL);
        historyLL = (LinearLayout) view.findViewById(R.id.historyLL);
        bankLL = (LinearLayout) view.findViewById(R.id.bankLL);
        editCard2 = (LinearLayout) view.findViewById(R.id.editCard2);

        addnewbank_RL = (RelativeLayout) view.findViewById(R.id.addnewbank_RL);
        addnewcardRL = (RelativeLayout) view.findViewById(R.id.addnewcardRL);
        topupRL = (RelativeLayout) view.findViewById(R.id.topupRL);
        sendmoneyRL = (RelativeLayout) view.findViewById(R.id.sendmoneyRL);
        requestRL = (RelativeLayout) view.findViewById(R.id.requestRL);
        withdralRL = (RelativeLayout) view.findViewById(R.id.withdralRL);

        overviewTV = (TextView) view.findViewById(R.id.overviewTV);
        historyTV = (TextView) view.findViewById(R.id.historyTV);
        bankTV = (TextView) view.findViewById(R.id.bankTV);
        editbankTV = (LinearLayout) view.findViewById(R.id.editbankTV);
        history_LV = (ListView) view.findViewById(R.id.history_LV1);
        bankScrollView = (ScrollView) view.findViewById(R.id.banklayout);

        walletHistory_adapter = new WalletHistoryAdapter(getActivity());
        history_LV.setAdapter(walletHistory_adapter);


        overviewLL.setOnClickListener(this);
        historyLL.setOnClickListener(this);
        bankLL.setOnClickListener(this);
        addnewbank_RL.setOnClickListener(this);
        editbankTV.setOnClickListener(this);
        addnewcardRL.setOnClickListener(this);
        topupRL.setOnClickListener(this);
        sendmoneyRL.setOnClickListener(this);
        requestRL.setOnClickListener(this);
        withdralRL.setOnClickListener(this);
        editCard2.setOnClickListener(this);

        return view;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.overviewLL:

                over_LL.setVisibility(View.VISIBLE);
                history_LV.setVisibility(View.GONE);
                bankScrollView.setVisibility(View.GONE);

                overviewLL.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                historyLL.setBackgroundColor(getResources().getColor(R.color.white));
                bankLL.setBackgroundColor(getResources().getColor(R.color.white));

                overviewTV.setTextColor(getResources().getColor(R.color.white));
                historyTV.setTextColor(getResources().getColor(R.color.black));
                bankTV.setTextColor(getResources().getColor(R.color.black));

                break;
            case R.id.historyLL:

                over_LL.setVisibility(View.GONE);
                history_LV.setVisibility(View.VISIBLE);
                bankScrollView.setVisibility(View.GONE);

                historyLL.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                overviewLL.setBackgroundColor(getResources().getColor(R.color.white));
                bankLL.setBackgroundColor(getResources().getColor(R.color.white));

                historyTV.setTextColor(getResources().getColor(R.color.white));
                overviewTV.setTextColor(getResources().getColor(R.color.black));
                bankTV.setTextColor(getResources().getColor(R.color.black));

                break;
            case R.id.bankLL:

                over_LL.setVisibility(View.GONE);
                history_LV.setVisibility(View.GONE);
                bankScrollView.setVisibility(View.VISIBLE);

                bankLL.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                overviewLL.setBackgroundColor(getResources().getColor(R.color.white));
                historyLL.setBackgroundColor(getResources().getColor(R.color.white));

                bankTV.setTextColor(getResources().getColor(R.color.white));
                overviewTV.setTextColor(getResources().getColor(R.color.black));
                historyTV.setTextColor(getResources().getColor(R.color.black));

                break;

            case R.id.addnewbank_RL:
                Intent intent = new Intent(getActivity(), AddNewBankActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;

            case R.id.editbankTV:
                Intent intent1 = new Intent(getActivity(), EditBankActivity.class);
                startActivity(intent1);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.addnewcardRL:
                Intent intent2 = new Intent(getActivity(), AddNewCardActivity.class);
                startActivity(intent2);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;

            case R.id.topupRL:
                Intent intent3 = new Intent(getActivity(), TopUpActivity.class);
                startActivity(intent3);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;

            case R.id.sendmoneyRL:
                Intent intent4 = new Intent(getActivity(), SendMoneyActivity.class);
                startActivity(intent4);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;

            case R.id.requestRL:
                Intent intent5 = new Intent(getActivity(), SendMoneyActivity.class);
                startActivity(intent5);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.withdralRL:

                Intent intent6 = new Intent(getActivity(), WithdrawlActivity.class);
                startActivity(intent6);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.editCard2:

                Intent intent7 = new Intent(getActivity(), EditCardActivity.class);
                startActivity(intent7);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
        }

    }
}
