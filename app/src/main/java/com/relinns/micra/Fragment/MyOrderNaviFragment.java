package com.relinns.micra.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.relinns.micra.Activity.MenuListActivity;
import com.relinns.micra.Activity.TrackOrder2Activity;
import com.relinns.micra.Adapter.MyOrderNaviAdapter;
import com.relinns.micra.Adapter.MyOrderNavi2Adapter;
import com.relinns.micra.R;

/**
 * Created by Relinns on 19-07-2017.
 */

public class MyOrderNaviFragment extends Fragment implements View.OnClickListener {
    ListView lv,lv1;
    MyOrderNaviAdapter naviAdapter;
    MyOrderNavi2Adapter naviAdapter2;

    TextView taxnavi1TV,taxnavi2TV,taxnavi3TV,listtext1TV,listtext2TV;
    LinearLayout restspace1LL,restspace2LL;
String value;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.myorder_navi_frag,container,false);
        ((MenuListActivity)getActivity()).settitleactivity("My OrderActivity");
        value="0";
        lv=(ListView)v.findViewById(R.id.list1);
        lv1=(ListView)v.findViewById(R.id.list2);

        taxnavi1TV=(TextView)v.findViewById(R.id.textnavi1);
        taxnavi2TV=(TextView)v.findViewById(R.id.textnavi2);
        taxnavi3TV=(TextView)v.findViewById(R.id.textnavi3);
        listtext1TV=(TextView)v.findViewById(R.id.listtext1);
        listtext2TV=(TextView)v.findViewById(R.id.listtext2);

        restspace1LL=(LinearLayout) v.findViewById(R.id.restspace1);
        restspace2LL=(LinearLayout) v.findViewById(R.id.restspace2);



        naviAdapter=new MyOrderNaviAdapter(getActivity(),value);
        lv.setAdapter(naviAdapter);



        taxnavi1TV.setOnClickListener(this);
        taxnavi2TV.setOnClickListener(this);
        taxnavi3TV.setOnClickListener(this);
        listtext1TV.setOnClickListener(this);


        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.textnavi1:
                value="0";
                taxnavi1TV.setVisibility(View.VISIBLE);
                restspace1LL.setVisibility(View.VISIBLE);
                listtext1TV.setVisibility(View.VISIBLE);
                restspace2LL.setVisibility(View.GONE);

                listtext2TV.setVisibility(View.VISIBLE);
                taxnavi1TV.setBackgroundColor(getResources().getColor(R.color.micra));
                taxnavi1TV.setTextColor(getResources().getColor(R.color.white));
                taxnavi2TV.setBackgroundColor(getResources().getColor(R.color.white));
                taxnavi2TV.setTextColor(getResources().getColor(R.color.black));
                taxnavi3TV.setBackgroundColor(getResources().getColor(R.color.white));
                taxnavi3TV.setTextColor(getResources().getColor(R.color.black));

                naviAdapter=new MyOrderNaviAdapter(getActivity(),value);
                lv.setAdapter(naviAdapter);
                break;

            case R.id.textnavi2:
                value="1";
                taxnavi2TV.setVisibility(View.VISIBLE);
                restspace1LL.setVisibility(View.VISIBLE);
                listtext1TV.setVisibility(View.GONE);
                restspace2LL.setVisibility(View.GONE);

                listtext2TV.setVisibility(View.GONE);
                taxnavi2TV.setBackgroundColor(getResources().getColor(R.color.micra));
                taxnavi2TV.setTextColor(getResources().getColor(R.color.white));
                taxnavi1TV.setBackgroundColor(getResources().getColor(R.color.white));
                taxnavi1TV.setTextColor(getResources().getColor(R.color.black));
                taxnavi3TV.setBackgroundColor(getResources().getColor(R.color.white));
                taxnavi3TV.setTextColor(getResources().getColor(R.color.black));

                naviAdapter=new MyOrderNaviAdapter(getActivity(),value);
                lv.setAdapter(naviAdapter);
                break;

            case R.id.textnavi3:
                taxnavi3TV.setVisibility(View.VISIBLE);
                restspace1LL.setVisibility(View.GONE);
                restspace2LL.setVisibility(View.VISIBLE);

                taxnavi3TV.setBackgroundColor(getResources().getColor(R.color.micra));
                taxnavi3TV.setTextColor(getResources().getColor(R.color.white));
                taxnavi2TV.setBackgroundColor(getResources().getColor(R.color.white));
                taxnavi2TV.setTextColor(getResources().getColor(R.color.black));
                taxnavi1TV.setBackgroundColor(getResources().getColor(R.color.white));
                taxnavi1TV.setTextColor(getResources().getColor(R.color.black));
                naviAdapter2=new MyOrderNavi2Adapter(getActivity());
                lv1.setAdapter(naviAdapter2);

                break;
            case R.id.listtext1:
                Intent i=new Intent(getActivity(), TrackOrder2Activity.class);
                startActivity(i);
                break;

        }
    }
}
