package com.relinns.micra.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.relinns.micra.Activity.MenuListActivity;
import com.relinns.micra.R;

/**
 * Created by Relinns Technologies on 03-07-2017.
 */
public class WebMicraFragment extends Fragment implements View.OnClickListener {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.webmicra_fragment,container,false);
        ((MenuListActivity) getActivity()).settitleactivity("Web Micra");

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){


        }
    }
}
