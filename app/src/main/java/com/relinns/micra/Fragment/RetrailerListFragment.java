package com.relinns.micra.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.relinns.micra.Adapter.RetralerListAdapter;
import com.relinns.micra.R;
import com.relinns.micra.model.RetrailerListModel;

import java.util.ArrayList;

/**
 * Created by Relinns on 20-07-2017.
 */

public class RetrailerListFragment extends Fragment {

    RetralerListAdapter adapter;
    ListView lv;
    ArrayList<RetrailerListModel> arrayList;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.retrailer_list_fragment,container,false);
        lv=(ListView)v.findViewById(R.id.list1);
        arrayList=new ArrayList<RetrailerListModel>();
        arrayList.add(new RetrailerListModel("Sai Medical Store","Payment:Mode,Code Wallet","3 km away",R.drawable.face1));
        arrayList.add(new RetrailerListModel("Sai Medical Store","Payment:Mode,Code Wallet","3 km away",R.drawable.face1));
        arrayList.add(new RetrailerListModel("Sai Medical Store","Payment:Mode,Code Wallet","3 km away",R.drawable.face1));
        arrayList.add(new RetrailerListModel("Sai Medical Store","Payment:Mode,Code Wallet","3 km away",R.drawable.face1));
        adapter=new RetralerListAdapter(getActivity(),arrayList);
        lv.setAdapter(adapter);
        return v;
    }
}
