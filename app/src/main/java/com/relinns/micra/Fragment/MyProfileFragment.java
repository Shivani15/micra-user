package com.relinns.micra.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.relinns.micra.Activity.MenuListActivity;
import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 30-06-2017.
 */
public class MyProfileFragment extends Fragment implements View.OnClickListener {

    Button StoreBtn,aboutBtn;
    RelativeLayout addnewRL;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.myprofile_fragment1,container,false);
        ((MenuListActivity) getActivity()).settitleactivity("My Profile");
        StoreBtn=(Button)view.findViewById(R.id.StoreLL);
        aboutBtn=(Button)view.findViewById(R.id.aboutLL);
      //  addnewRL=(RelativeLayout)view.findViewById(R.id.addnewRL);

        StoreBtn.setOnClickListener(this);
        aboutBtn.setOnClickListener(this);
      //  addnewRL.setOnClickListener(this);

        Fragment fm = new OnlyProfileFragment();
        FragmentTransaction ftr = getFragmentManager().beginTransaction();
        ftr.replace(R.id.profileframeFL,fm,null);
        ftr.commit();
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.aboutLL:


                aboutBtn.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                StoreBtn.setBackgroundColor(getResources().getColor(R.color.white));
                aboutBtn.setTextColor(getResources().getColor(R.color.white));
                StoreBtn.setTextColor(getResources().getColor(R.color.black));


                Fragment fm = new OnlyProfileFragment();
                FragmentTransaction ftr = getFragmentManager().beginTransaction();
                ftr.replace(R.id.profileframeFL,fm,null);
                ftr.commit();


                break;
            case R.id.StoreLL:

                StoreBtn.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                aboutBtn.setBackgroundColor(getResources().getColor(R.color.white));
                StoreBtn.setTextColor(getResources().getColor(R.color.white));
                aboutBtn.setTextColor(getResources().getColor(R.color.black));



                Fragment fm1 = new OnlyStoreDetailFragment();
                FragmentTransaction ftr1 = getFragmentManager().beginTransaction();
                ftr1.replace(R.id.profileframeFL,fm1,null);
                ftr1.commit();

                break;

          /*  case R.id.addnewRL:
                Intent intent1=new Intent(getActivity(),AddProfile_Address_Activity.class);
                startActivity(intent1);

                break;*/
        }

    }
}
