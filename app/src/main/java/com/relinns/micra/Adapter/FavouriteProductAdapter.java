package com.relinns.micra.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.relinns.micra.Activity.ProductDetailsActivity;
import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 01-07-2017.
 */
public class FavouriteProductAdapter extends BaseAdapter {
    Context context;
    Activity activity;
    public FavouriteProductAdapter(FragmentActivity context) {
        this.context=context;
        activity=(Activity)context;

    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder = new Holder();
        View rowView;
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.favproduct_list_item, parent, false);
         holder.addtocartfav=(RelativeLayout)convertView.findViewById(R.id.add_to_cart_fav);
        holder.addtocartfav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,ProductDetailsActivity.class);
                intent.putExtra("image","1");
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });






        return convertView;
    }

    public class Holder {
        RelativeLayout addtocartfav;

    }
}

