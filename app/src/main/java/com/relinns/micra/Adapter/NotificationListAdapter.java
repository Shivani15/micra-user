package com.relinns.micra.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.relinns.micra.Activity.NotificationListActivity;
import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 04-07-2017.
 */
public class NotificationListAdapter extends BaseAdapter {
    Context context;
    public NotificationListAdapter(NotificationListActivity context) {
        this.context=context;

    }

    @Override
    public int getCount() {
        return 7;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView;
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.notificationlist_item, parent, false);
        LinearLayout rit=(LinearLayout)convertView.findViewById(R.id.noti1);
        rit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i=new Intent(context,ReportUserActivity.class);
//                context.startActivity(i);
            }
        });
        return convertView;
    }


}
