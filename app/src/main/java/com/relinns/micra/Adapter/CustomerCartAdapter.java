package com.relinns.micra.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.relinns.micra.Activity.CartActivity;
import com.relinns.micra.R;
import com.relinns.micra.model.CartModel;

import java.util.ArrayList;

/**
 * Created by Relinns on 25-07-2017.
 */

public class CustomerCartAdapter extends RecyclerView.Adapter<CustomerCartAdapter.View_holder> {


    CartActivity context;
    ArrayList<CartModel> list;
    public CustomerCartAdapter(CartActivity customer_cart, ArrayList<CartModel> cart_list) {
        this.context=customer_cart;
        this.list=cart_list;
    }

    @Override
    public View_holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(context).inflate(R.layout.design_customer_cart,parent,false);
        return new View_holder(v);
    }

    @Override
    public void onBindViewHolder(View_holder holder, int position) {

        holder.cart_productName.setText(list.get(position).getProduct_name());
        holder.cart_vendorName.setText(list.get(position).getVendor_name());
        holder.cart_productPrice.setText(list.get(position).getPrice());
        holder.cart_productOffer.setText(list.get(position).getOffer());
        holder.cart_quantity.setText(list.get(position).getQuantity());
        holder.cart_image.setImageResource(list.get(position).getPlace_holder());
    }



    @Override
    public int getItemCount() {
        return list.size();
    }

    public class View_holder extends RecyclerView.ViewHolder {
        ImageView cart_image;
        TextView cart_productName, cart_vendorName, cart_productPrice, cart_productOffer, cart_quantity;
        public View_holder(View itemView) {
            super(itemView);
            cart_image=(ImageView)itemView.findViewById(R.id.cart_image);
            cart_productName=(TextView)itemView.findViewById(R.id.cart_productName);
            cart_productOffer=(TextView)itemView.findViewById(R.id.cart_productOffer);
            cart_vendorName=(TextView)itemView.findViewById(R.id.cart_vendorName);
            cart_productPrice=(TextView)itemView.findViewById(R.id.cart_productPrice);
            cart_quantity=(TextView)itemView.findViewById(R.id.cart_quantity);
        }
    }
}
