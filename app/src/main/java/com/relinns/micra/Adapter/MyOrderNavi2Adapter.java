package com.relinns.micra.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.relinns.micra.Activity.OrderActivity;
import com.relinns.micra.R;

/**
 * Created by Relinns on 19-07-2017.
 */

public class MyOrderNavi2Adapter extends BaseAdapter {

    Context context;

    public MyOrderNavi2Adapter(Context ct) {
        this.context=ct;
    }

    @Override
    public int getCount() {

        return 4;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=(LayoutInflater.from(context));
        View v=inflater.inflate(R.layout.myorder_navi_fragment_child2,parent ,false);
        LinearLayout l1=(LinearLayout)v.findViewById(R.id.search_navi);
        l1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent i=new Intent(context,OrderActivity.class);
                context.startActivity(i);
            }
        });

        return v;
    }
}
