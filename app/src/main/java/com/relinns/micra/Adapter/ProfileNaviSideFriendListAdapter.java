package com.relinns.micra.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.relinns.micra.Activity.Main3Activity;
import com.relinns.micra.R;
import com.relinns.micra.model.ProfileNaviFriendListModel;

import java.util.ArrayList;

/**
 * Created by Relinns on 28-07-2017.
 */

public class ProfileNaviSideFriendListAdapter extends BaseAdapter {
    Context ct;
    ArrayList<ProfileNaviFriendListModel> list1;

    public ProfileNaviSideFriendListAdapter(Context ct, ArrayList<ProfileNaviFriendListModel> list1) {
        this.ct = ct;
        this.list1 = list1;
    }

    @Override

    public int getCount() {
        return list1.size();
    }



    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=(LayoutInflater.from(ct));
        View v=inflater.inflate(R.layout.profile_navi_side_friend_list_adapter,null,true);
        ImageView image1=(ImageView)v.findViewById(R.id.img1);
        TextView john=(TextView)v.findViewById(R.id.first);
        TextView mohali=(TextView)v.findViewById(R.id.second);
        LinearLayout item_list_freind=(LinearLayout)v.findViewById(R.id.item_list_freind);
        item_list_freind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ct,Main3Activity.class);
                ct.startActivity(i);
            }
        });
        image1.setImageResource(list1.get(position).getImage1());
        mohali.setText(list1.get(position).getMohali());
        john.setText(list1.get(position).getJohn());

        return v;
    }}

