package com.relinns.micra.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.relinns.micra.Activity.ProductDetailsActivity;
import com.relinns.micra.R;
import com.relinns.micra.model.BrowseProductModel;

import java.util.ArrayList;

/**
 * Created by Relinns on 20-07-2017.
 */

public class BrowseProductAdapter extends BaseAdapter {
    ArrayList<BrowseProductModel> list1;
    Context ct;

    public BrowseProductAdapter(ArrayList<BrowseProductModel> list1, Context ct) {
        this.list1 = list1;
        this.ct = ct;
    }

    @Override
    public int getCount() {
        return list1.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=(LayoutInflater.from(ct));
        View v=inflater.inflate(R.layout.browse_product_list_item,parent,false);
        TextView product1=(TextView)v.findViewById(R.id.Productname);
        TextView price=(TextView)v.findViewById(R.id.Price);
        ImageView img=(ImageView) v.findViewById(R.id.image_profile);
        product1.setText(list1.get(position).getProduct());
        price.setText(list1.get(position).getPrice());
        img.setImageResource(list1.get(position).getImage());
        Button cartvendor=(Button)v.findViewById(R.id.cart_Vendor);
        cartvendor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ct, ProductDetailsActivity.class);
                ct.startActivity(i);
            }
        });
        return v;
    }
}
