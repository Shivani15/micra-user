package com.relinns.micra.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra.R;
import com.relinns.micra.model.CustomerModel;

import java.util.ArrayList;

/**
 * Created by admin on 06-07-2017.
 */
public class MyCustomerAdapter extends RecyclerView.Adapter<MyCustomerAdapter.ViewHolder> {
    Context context;
    ArrayList<CustomerModel> list;
    public MyCustomerAdapter(Context my_customers, ArrayList<CustomerModel> customer_data) {
        this.context=my_customers;
        this.list=customer_data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(context).inflate(R.layout.design_customers,parent,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
       holder.user_image.setImageResource(list.get(position).getOffline_user());
        holder.user_name.setText(list.get(position).getUser_name());
        holder.user_country.setText(list.get(position).getPlace());
        if(list.get(position).getOrder().equals("0"))
        {
            holder.place_order.setVisibility(View.GONE);
        }
        else
        {
            holder.place_order.setVisibility(View.VISIBLE);
        }
    }



    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView user_image;
        TextView user_name, user_country;
        RelativeLayout place_order, chat;
        public ViewHolder(View itemView) {
            super(itemView);
            user_image=(ImageView)itemView.findViewById(R.id.user_image);
            user_name=(TextView)itemView.findViewById(R.id.customer_name);
            user_country=(TextView)itemView.findViewById(R.id.customer_city);
            place_order=(RelativeLayout)itemView.findViewById(R.id.place_order);
            chat=(RelativeLayout)itemView.findViewById(R.id.chat);



        }
    }
}
