package com.relinns.micra.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.relinns.micra.R;
import com.relinns.micra.model.MessageModel;

import java.util.ArrayList;


/**
 * Created by intel on 6/30/2017.
 */

public class MessageAdapter extends BaseAdapter {
    Context context;
    ArrayList<MessageModel> list1;
    public MessageAdapter(Context context, ArrayList<MessageModel> list1)
    {
        this.context=context;
        this.list1=list1;
    }

    @Override
    public int getCount()
    {
        return list1.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.employeelist_item, parent, false);
        ImageView justTV1= (ImageView)convertView.findViewById(R.id.profileIV);
        TextView justTV2= (TextView)convertView.findViewById(R.id.top1);
        TextView justTV3= (TextView)convertView.findViewById(R.id.top2);
          justTV1.setImageResource(list1.get(position).getOffline_user());
        justTV2.setText(list1.get(position).getPlace());
        justTV3.setText(list1.get(position).getUser_name());


        return convertView;
    }

}
