package com.relinns.micra.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.relinns.micra.Activity.TopUpActivity;
import com.relinns.micra.R;

import static android.content.ContentValues.TAG;

/**
 * Created by Relinns on 21-07-2017.
 */
public class MyAdapter extends ArrayAdapter {
    Context ct;
    String[] s1;
    String[] s2;
    Integer[] img;
    public MyAdapter(Context ct, String[] s1, String[] s2, Integer[] img) {
        super(ct, R.layout.activity8_adapter,s1);
        this.ct=ct;
        this.s1=s1;
        this.s2=s2;
        this.img=img;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(ct);
        View listViewItem = inflater.inflate(R.layout.activity8_adapter,null,true);
        TextView textViewId = (TextView) listViewItem.findViewById(R.id.first);
        TextView textViewName = (TextView) listViewItem.findViewById(R.id.second);
        ImageView img1=(ImageView)listViewItem.findViewById(R.id.img1);
        Button addtocart=(Button)listViewItem.findViewById(R.id.cart_act8);
        textViewId.setText(s1[position]);
        textViewName.setText(s2[position]);
        img1.setImageResource(img[position]);
        addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ct,TopUpActivity.class);
                ct.startActivity(i);
                Log.d(TAG, "onClick: hlo");
            }
        });
        return listViewItem;
    }
}


