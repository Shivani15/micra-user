package com.relinns.micra.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.relinns.micra.Activity.ProductDetailsActivity;
import com.relinns.micra.R;
import com.relinns.micra.model.VendorProfileModel;

import java.util.ArrayList;

/**
 * Created by knl on 7/12/2017.
 */

public class VendorProfileAdapter extends BaseAdapter {
  Context context;
    ArrayList<VendorProfileModel> list1;
    public VendorProfileAdapter(Context ct, ArrayList<VendorProfileModel> list1) {
        this.context=ct;
        this.list1=list1;
    }

    @Override
    public int getCount() {
        return list1.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=(LayoutInflater.from(context));
        View v=inflater.inflate(R.layout.vendor_profile_adapter,null,true);
        TextView product=(TextView)v.findViewById(R.id.Product_Profile);
        TextView price=(TextView)v.findViewById(R.id.Product_Price);
        ImageView image2=(ImageView)v.findViewById(R.id.image_profile);
        Button cart1=(Button)v.findViewById(R.id.cart_Vendor);


        image2.setImageResource(list1.get(position).getImage());

        product.setText(list1.get(position).getProduct());
        price.setText(list1.get(position).getPrice());
        cart1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context,ProductDetailsActivity.class);
                context.startActivity(i);
            }
        });

        return v;
    }
}
