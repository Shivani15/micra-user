package com.relinns.micra.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.relinns.micra.Activity.VendorProfileActivity;
import com.relinns.micra.R;
import com.relinns.micra.model.VendorModel;

import java.util.ArrayList;


import static android.content.ContentValues.TAG;

/**
 * Created by knl on 7/11/2017.
 */

public class SearchVendorAdapter extends BaseAdapter {
    Context context;
    ArrayList<VendorModel> list;
    public SearchVendorAdapter(Context search_vendor, ArrayList<VendorModel> vendorlist) {
     context=search_vendor;
        list=vendorlist;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    Button View_Profile;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=(LayoutInflater.from(context));
        View v=inflater.inflate(R.layout.search_vendor_adapter,null,true);
        TextView Sai=(TextView)v.findViewById(R.id.sai1);
        TextView distance=(TextView)v.findViewById(R.id.km1);
        TextView Payment=(TextView)v.findViewById(R.id.pay);
        ImageView image=(ImageView)v.findViewById(R.id.image1);
         View_Profile=(Button)v.findViewById(R.id.profile1);


        image.setImageResource(list.get(position).getImage());

        Sai.setText(list.get(position).getName());
        distance.setText(list.get(position).getDistance());
        Payment .setText(list.get(position).getPayment());
        View_Profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context,VendorProfileActivity.class);
                context.startActivity(i);
                Log.d(TAG, "onClick: hlo");
            }
        });
        return v;
    }
}
