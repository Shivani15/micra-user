package com.relinns.micra.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.relinns.micra.Activity.VendorProfileActivity;
import com.relinns.micra.R;
import com.relinns.micra.model.BrowserVendorModel;

import java.util.ArrayList;

/**
 * Created by Relinns on 20-07-2017.
 */

public class BrowserVendorAdapter extends BaseAdapter {
    Context context;
    ArrayList<BrowserVendorModel> list;

    public BrowserVendorAdapter(Context context, ArrayList<BrowserVendorModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater=(LayoutInflater.from(context));
        View v=inflater.inflate(R.layout.browser_vendor_adapter,parent,false);
        TextView Sai=(TextView)v.findViewById(R.id.sai1);
        TextView distance=(TextView)v.findViewById(R.id.km1);
        TextView Payment=(TextView)v.findViewById(R.id.pay);
        ImageView image=(ImageView)v.findViewById(R.id.image1);


        image.setImageResource(list.get(position).getImage());

        Sai.setText(list.get(position).getName());
        distance.setText(list.get(position).getDistance());
        Payment .setText(list.get(position).getPayment());
        Button b1=(Button)v.findViewById(R.id.profile1_vendor);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context,VendorProfileActivity.class);
                context.startActivity(i);
            }
        });


        return v;
    }
}
