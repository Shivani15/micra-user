package com.relinns.micra.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.relinns.micra.Activity.BlockedUserActivity;
import com.relinns.micra.Activity.ReportUserActivity;
import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 04-07-2017.
 */
public class BlockUsersAdapter extends BaseAdapter {
    Context context;
    Activity activity;
    public BlockUsersAdapter(BlockedUserActivity context) {
        this.context=context;
        activity=(Activity)context;

    }


    @Override
    public int getCount() {
        return 7;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.blockuser_item, parent, false);
        holder.reportuser_TV1=(TextView)convertView.findViewById(R.id.saiblock1);
        holder.reportuser_TV1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,ReportUserActivity.class);
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });



        return convertView;
    }

    public class Holder {
        TextView reportuser_TV1;

    }
}
