package com.relinns.micra.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra.Activity.OrderActivity;
import com.relinns.micra.Activity.TrackOrder2Activity;
import com.relinns.micra.R;

/**
 * Created by Relinns on 19-07-2017.
 */

public class MyOrderNaviAdapter extends BaseAdapter{
    Context ct;
    String s1;
    public MyOrderNaviAdapter(Context ct, String value) {
        this.ct=ct;
        this.s1=value;
    }

    @Override
    public int getCount() {

        return 6;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=(LayoutInflater.from(ct));
        View v=inflater.inflate(R.layout.myorder_navi_frag_child,null ,true);
        TextView t1=(TextView)v.findViewById(R.id.orderno_);
        RelativeLayout rel=(RelativeLayout)v.findViewById(R.id.trackonmap1);
        LinearLayout rel2=(LinearLayout)v.findViewById(R.id.retorderno1);
        Button trackonmap_intransit=(Button)v.findViewById(R.id.trackonmap_intransit);
        if(s1.equals("0"))
        {
             rel.setVisibility(v.GONE);
        }
        else
        {
            rel.setVisibility(v.VISIBLE);

        }
        trackonmap_intransit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ct, TrackOrder2Activity.class);
                ct.startActivity(i);
            }
        });
        rel2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ct,OrderActivity.class);
               ct.startActivity(i);
            }
        });

        return v;
    }
}
