package com.relinns.micra.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.relinns.micra.R;

/**
 * Created by Relinns on 21-07-2017.
 */

public class Main6Adapter extends ArrayAdapter {
    Context ct;
    String[] s1;
    String[] s2;
    Integer[] img;
    public Main6Adapter(Context ct, String[] s1, String[] s2, Integer[] img) {
        super(ct, R.layout.main6_adapter,s1);
        this.ct=ct;
        this.s1=s1;
        this.s2=s2;
        this.img=img;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(ct);
        View listViewItem = inflater.inflate(R.layout.main6_adapter,null,true);
        TextView textViewId = (TextView) listViewItem.findViewById(R.id.textView1);
        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textView2);
        ImageView img1=(ImageView)listViewItem.findViewById(R.id.imageView1);

        textViewId.setText(s1[position]);
        textViewName.setText(s2[position]);
        img1.setImageResource(img[position]);
        return listViewItem;
    }
}
