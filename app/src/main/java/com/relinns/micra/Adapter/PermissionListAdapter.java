package com.relinns.micra.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.relinns.micra.Activity.PermissionListActivity;
import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 04-07-2017.
 */
public class PermissionListAdapter extends BaseAdapter {
    Context context;
    public PermissionListAdapter(PermissionListActivity context) {
        this.context=context;


    }

    @Override
    public int getCount() {
        return 7;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.permissionlist_item, parent, false);


        return convertView;
    }

    public class Holder {

    }
}
