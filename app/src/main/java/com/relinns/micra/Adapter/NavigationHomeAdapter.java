package com.relinns.micra.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.relinns.micra.Activity.MenuListActivity;
import com.relinns.micra.R;
import com.relinns.micra.model.NavigationHomeModel;

import java.util.ArrayList;

/**
 * Created by Relinns Technologies on 19-06-2017.
 */
public class NavigationHomeAdapter extends BaseAdapter {

    ArrayList<NavigationHomeModel> navigationHome_models;
    Context context;
    public NavigationHomeAdapter(ArrayList<NavigationHomeModel> navigationHome_models, MenuListActivity context) {
        this.navigationHome_models=navigationHome_models;
        this.context=context;
    }

    @Override
    public int getCount() {
        return navigationHome_models.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

     @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder = new Holder();
        View rowView;
        //  rowView = inflater.inflate(R.layout.modelnavigation, null);
        // rowView = inflater.inflate(R.layout.modelnavigation,null,false);
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.navigation_model, parent, false);
        holder.tv = (TextView) convertView.findViewById(R.id.textTV);
        holder.img = (ImageView) convertView.findViewById(R.id.imageIV);
        holder.switchbutton = (Switch) convertView.findViewById(R.id.switchbutton);



        String name = navigationHome_models.get(position).getName();
        int image = navigationHome_models.get(position).getImage();
        holder.tv.setText(name);
        holder.img.setImageResource(image);
        return convertView;
    }

    public class Holder {
        TextView tv;
        ImageView img;
        Switch switchbutton;
    }
}


