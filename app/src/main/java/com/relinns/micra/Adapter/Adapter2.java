package com.relinns.micra.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.relinns.micra.Activity.MainActivity;
import com.relinns.micra.R;

/**
 * Created by Relinns on 21-07-2017.
 */

public class Adapter2 extends BaseAdapter {
    String[] s1;
    String[] s2;
    int[] image;
    Context ct;

    public Adapter2(Context ct, int[] image,String[] s1, String[] s2) {
        this.s1=s1;
        this.s2=s2;
        this.image=image;
        this.ct=ct;

    }

    @Override
    public int getCount() {
        return s1.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater=(LayoutInflater)ct.getSystemService(ct.LAYOUT_INFLATER_SERVICE);

        View v=inflater.inflate(R.layout.fragment2_child,null);



        TextView txt1 = (TextView) v.findViewById(R.id.first);
        TextView txt2 = (TextView) v.findViewById(R.id.second);
        Button b1= (Button) v.findViewById(R.id.accept2);

        txt1.setText(s1[position]);
        txt2.setText(s2[position]);

        ImageView img = (ImageView) v.findViewById(R.id.img1);
        img.setImageResource(image[position]);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ct,MainActivity.class);
                ct.startActivity(i);

            }
        });


        return v;}
}
