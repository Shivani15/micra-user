package com.relinns.micra.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.relinns.micra.Activity.ChatActivity;
import com.relinns.micra.R;
import com.relinns.micra.model.UserModel;

import java.util.ArrayList;

/**
 * Created by knl on 7/11/2017.
 */

public class SearchUserAdapter extends BaseAdapter {

    Context context;
    ArrayList<UserModel> list;
    public SearchUserAdapter(Context search_vendor, ArrayList<UserModel> userlist) {
        context=search_vendor;
        list=userlist;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=(LayoutInflater.from(context));
        View v=inflater.inflate(R.layout.search_user_adapter,null,true);
        TextView Sai=(TextView)v.findViewById(R.id.user_sai1);
        TextView distance=(TextView)v.findViewById(R.id.user_km1);
        TextView Payment=(TextView)v.findViewById(R.id.user_pay);
        ImageView image=(ImageView)v.findViewById(R.id.user_image1);
        Button Chat=(Button)v.findViewById(R.id.chat_button);

        image.setImageResource(list.get(position).getImage());

        Sai.setText(list.get(position).getName());
        distance.setText(list.get(position).getDistance());
        Payment .setText(list.get(position).getPayment());
        Chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Intent i=new Intent(context,ChatActivity.class);
               context.startActivity(i);
            }
        });
        return v;
    }}
